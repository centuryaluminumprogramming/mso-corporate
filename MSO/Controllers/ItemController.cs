﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MSO.DataAccess;
using MSO.Models;
using System;
using System.Collections.Generic;

namespace MSO.Controllers
{
    public class ItemController : Controller
    {
        private string _MsoConnectionString;
        private readonly IConfiguration _config;

        public ItemController(IConfiguration config)
        {
            _config = config;
        }

        // GET: ItemController
        public ActionResult Index(Mso mso)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            IList<Item> items = sQLHelper.GetMsoItems(mso.RequestNumber);

            return View(items);
        }

        // GET: ItemController/Details/5
        public ActionResult Details()
        {
            return View();
        }

        // GET: ItemController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ItemController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ItemController/Edit/5
        public ActionResult Edit()
        {
            return View();
        }

        // POST: ItemController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ItemController/Delete/5
        public ActionResult Delete()
        {
            return View();
        }

        // POST: ItemController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult RemoveItem(int ItemCount, string RequestNumber)

        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            sQLHelper.RemoveItemFromMso(ItemCount, RequestNumber);

            return RedirectPermanentPreserveMethod(Request.Headers["Referer"].ToString());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddItem(int RequestNumber, string StockNumber, string Description, double Quantity, string Unit, double Gross, double Tare, double Net, string Value, List<string> item)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            List<Item> items = new List<Item>(sQLHelper.GetMsoItems(RequestNumber));

            try
            {
                Mso mso = new Mso();
                int ItemCount = 0;

                if (items.Count == 0)
                {
                    ItemCount = 1;
                }
                else
                {
                    ItemCount = items.Count + 1;
                }

                sQLHelper.AddItemToMso(RequestNumber, StockNumber, Description, Quantity, Unit, Gross, Tare, Net, Value, ItemCount);

                if (Request.Headers["Referer"].ToString().Contains("edit"))
                {
                    return RedirectToAction("Edit", "Mso", new { id = RequestNumber });
                }
                else
                {
                    return RedirectToAction("Create", "Mso", new { id = RequestNumber });
                }
            }
            catch
            {
                return View("Edit", "Mso");
            }
        }
    }
}