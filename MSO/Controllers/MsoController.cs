﻿using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using MSO.DataAccess;
using MSO.Models;
using MSO.Tools;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using X.PagedList;

namespace MSO.Controllers
{
    public class MsoController : Controller
    {
        private readonly IConfiguration _config;
        private static readonly StringBuilder sb = new StringBuilder();

        public MsoController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet]
        public IActionResult Adhoc()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Adhoc(string adhoc, string msoNumber, string StartDate, string EndDate, string file)
        {
            Files utilities = new Files(_config["MsoSQLConnection"]);
            Mso mso = new Mso();
            List<Mso> msos = new List<Mso>();

            bool hasData = false;

            if (StartDate != null && EndDate != null)
            {
                hasData = (utilities.DownloadAdhocFile(adhoc, msoNumber, StartDate, EndDate, User.Identity.Name, file) != false) ? true : false;

                if (hasData == true)
                {
                    return RedirectToAction("FileDownloaded", "Mso", file);
                }
                else
                {
                    mso.Status = "No data was returned.";
                    msos.Add(mso);
                    return View(msos);
                }
            }

            return View();
        }

        public ActionResult FileDownloaded(string fileName)
        {
            return View(fileName);
        }

        public IActionResult UserName()
        {
            User storeUser = new User();
            storeUser.UserName = User.Identity.Name;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddItem(int RequestNumber, string StockNumber, string Description, double Quantity, string Unit, double Gross, double Tare, double Net, string Value)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            List<Item> items = new List<Item>(sQLHelper.GetMsoItems(RequestNumber));

            try
            {
                Mso mso = new Mso();
                int ItemCount = 0;

                if (items.Count == 0)
                {
                    ItemCount = 1;
                }
                else
                {
                    ItemCount = items.Count + 1;
                }

                sQLHelper.AddItemToMso(RequestNumber, StockNumber, Description, Quantity, Unit, Gross, Tare, Net, Value, ItemCount);
                if (Request.Headers["Referer"].ToString().Contains("Edit"))
                {
                    return RedirectToAction("Edit", "Mso", new { id = RequestNumber });
                }
                else
                {
                    return RedirectToAction("Create", "Mso", new { id = RequestNumber });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return View("Edit", "Mso");
            }

            /// TODO Add error handling
        }

        // GET: MsoController
        public ActionResult Index(int? page, string sortOrder)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            Users utilities = new Users(_config["MsoSQLConnection"]);
            User user = new User();
            user = utilities.GetUserInfo(User.Identity.Name);

            if (string.IsNullOrEmpty(user.Plant))
            {
                return RedirectToAction("SelectPlant");
            }
            else
            {
                IList<Mso> msos = sQLHelper.GetMsos(User.Identity.Name);

                ViewBag.CurrentSort = sortOrder;
                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "RequestNumber" : "";

                int pageSize = 50;
                int pageNumber = page ?? 1;

                pageNumber = (pageNumber != 0) ? pageNumber : 1;
                return View(msos.ToPagedList(pageNumber, pageSize));
            }
        }

        // GET: MsoController/Details/5
        public ActionResult Details(int id)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            Shipping shipping = new Shipping();
            Vendor vendor = new Vendor();
            Mso mso = sQLHelper.GetMso(id, User.Identity.Name);

            mso.Items = sQLHelper.GetMsoItems(id);

            return View(mso);
        }

        // GET: MsoController/Create
        [HttpGet]
        public ActionResult Create()
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            User user = new User();
            Users utilities = new Users(_config["MsoSQLConnection"]);

            try
            {
                Mso mso = new Mso
                {
                    RequestNumber = sQLHelper.GetNextRequestNumber(),
                    Items = sQLHelper.GetMsoItems(sQLHelper.GetNextRequestNumber()),
                    Vendors = sQLHelper.GetVendors(User.Identity.Name),
                    Plants = sQLHelper.GetPlants()
                };

                user = utilities.GetUserInfo(User.Identity.Name);

                if (string.IsNullOrEmpty(user.Plant))
                {
                    return RedirectToAction("SelectPlant");
                }

                if (user.RoleId != 0)
                {
                    return View(mso);
                }
                else
                {
                    return RedirectToAction("Unauthorized", "Shared");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return View();
            }
        }

        [HttpPost]
        public string UpdatePlant(string plant)
        {
            User user = new User();
            Users users = new Users(_config["MsoSQLConnection"]);
            Plants plants = new Plants(_config["MsoSQLConnection"]);
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            var page = Request.Headers["Referer"].ToString();

            try
            {
                user = users.GetUserInfo(User.Identity.Name);

                if (user.GUID == null)
                {
                    user.Plant = plant;
                    user.UserName = User.Identity.Name;
                    user.GUID = users.GetUserGUID(user.UserName);
                    user.RoleId = 0;

                    users.InsertUser(user);
                }
                else
                {
                    Plants.UpdatePlant(plant, user);
                }

                Mso mso = new Mso
                {
                    RequestNumber = sQLHelper.GetNextRequestNumber()
                };
                mso.RequestedByName = User.Identity.Name;

                page = (page.Contains("SelectPlant")) ? Request.Headers["Origin"].ToString() : Request.Headers["Referer"].ToString();
                return page;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return page;
            }
        }

        public ActionResult SelectPlant()
        {
            Mso mso = new Mso();
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            mso.Plants = sQLHelper.GetPlants();
            return View(mso);
        }

        // POST: MsoController/Create
        [HttpPost]
        public ActionResult Create(Mso mso)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            string userName;
            userName = User.Identity.Name;
            userName = userName.ToString().Substring(8, userName.Length - 8);
            try
            {
                mso.RequestedByName = userName;
                mso.RequestNumber = sQLHelper.GetNextRequestNumber();
                mso.RoleId = sQLHelper.GetRoleId(userName);

                if (mso.RoleId != 0)
                {
                    sQLHelper.CreateMso(mso);

                    mso.Items = sQLHelper.GetMsoItems(mso.RequestNumber);
                    return RedirectToAction("Edit", "Mso", new { id = mso.RequestNumber });
                }
                else
                {
                    return RedirectToAction("Unauthorized", "Shared");
                }

                //return RedirectToAction("Create", "Mso", new { id = mso.RequestNumber });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return View();
            }
        }

        // GET: MsoController/Edit/5
        public ActionResult Edit(int id)
        {
            Users utilities = new Users(_config["MsoSQLConnection"]);
            User user = new User();

            user = utilities.GetUserInfo(User.Identity.Name);
            if (user.RoleId != 0)
            {
                SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
                Mso mso = sQLHelper.GetMso(id, User.Identity.Name);

                mso.RequestNumber = id;
                mso.Vendor = sQLHelper.GetVendorByRequest(id);
                mso.Items = sQLHelper.GetMsoItems(id);

                return View(mso);
            }
            else
            {
                return RedirectToAction("Unauthorized", "Shared");
            }
        }

        public ActionResult Print(IFormCollection collection)
        {
            try
            {
                string pdfTemplate = @"c:\Temp\ViewReport.pdf";
                string newFile = @"c:\Temp\completed_Report.pdf";
                string msoNumber = "";
                string returnbool = "";
                string[] number = Request.Form["RequestNumber"];
                string[] returnstatus = Request.Form["Return"];
                bool ourPlant = false;
                bool destination = false;
                bool otherSpecify = false;

                bool charge = false;
                bool allow = false;
                bool collect = false;

                number = number[0].Split(",");
                msoNumber = number[0].ToString();

                returnstatus = returnstatus[0].Split(",");
                returnbool = returnstatus[0].ToString();

                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                AcroFields pdfFormFields = pdfStamper.AcroFields;

                var shipToExtended = Request.Form["Vendor.City"] + ", " + Request.Form["Vendor.State"] + ", " + Request.Form["Vendor.Zip"];
                var soldToExtended = Request.Form["SoldTo3"] + ", " + Request.Form["SoldTo4"];

                if (!string.IsNullOrEmpty(Request.Form["Fob"].ToString()))
                {
                    switch (Request.Form["Fob"].ToString())
                    {
                        case "1":
                            ourPlant = true;
                            break;

                        case "2":
                            destination = true;
                            break;

                        case "3":
                            otherSpecify = true;
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(Request.Form["freight"].ToString()))
                {
                    switch (Request.Form["freight"].ToString())
                    {
                        case "1":
                            charge = true;
                            break;

                        case "2":
                            allow = true;
                            break;

                        case "3":
                            collect = true;
                            break;
                    }
                }

                bool cash = Convert.ToInt32(Request.Form["TransactionType"]) == 1 ? true : false;
                bool credit = Convert.ToInt32(Request.Form["TransactionType"]) == 2 ? true : false;
                bool creditMemo = Convert.ToInt32(Request.Form["TransactionType"]) == 3 ? true : false;
                bool gatePass = Convert.ToInt32(Request.Form["TransactionType"]) == 4 ? true : false;

                bool vendorReturn = Request.Form["Return"] == true ? true : false;
                bool repaired = Request.Form["Repaired"] == true ? true : false;
                bool goods = Request.Form["GoodsShipped"] == true ? true : false;
                bool loan = Request.Form["GoodsLoaned"] == true ? true : false;
                bool removed = Request.Form["VendorRemoved"] == true ? true : false;
                bool other = Request.Form["Other"] == true ? true : false;

                var creationDate = (!string.IsNullOrEmpty(Request.Form["CreationDate"])) ? Convert.ToDateTime(Request.Form["CreationDate"]).ToShortDateString() : Convert.ToDateTime(Request.Form["ShipDate"]).ToShortDateString();

                pdfFormFields.SetField("PLANT CODE", Request.Form["Plant"]);
                pdfFormFields.SetField("MSO NO", msoNumber.ToString());
                pdfFormFields.SetField("MSO DATE", creationDate);
                pdfFormFields.SetField("MSO SHIP DATE", Convert.ToDateTime(Request.Form["ShipDate"]).ToShortDateString());

                pdfFormFields.SetField("Purchase Order NO", Request.Form["PurchaseNumber"]);
                pdfFormFields.SetField("Other Ref NO", Request.Form["OtherReferenceNumber"]);
                pdfFormFields.SetField("Work Order Number", Request.Form["WorkOrderNumber"]);

                pdfFormFields.SetField("REQ NO", Request.Form["OrderNumer"]);
                pdfFormFields.SetField("BILL OF LADING NO", Request.Form["BillOfLading"]);

                pdfFormFields.SetField("SHIP VIA", Request.Form["ShipVia"]);
                pdfFormFields.SetField("SHIP NAME", Request.Form["Vendor.Name"]);
                pdfFormFields.SetField("SHIP ADDRESS", Request.Form["Vendor.Address"]);
                pdfFormFields.SetField("SHIP EXTENDED", shipToExtended);

                pdfFormFields.SetField("SOLD NAME", Request.Form["SoldTo1"]);
                pdfFormFields.SetField("SOLD ADDRESS", Request.Form["SoldTo2"]);
                pdfFormFields.SetField("SOLD EXTENDED", soldToExtended);

                pdfFormFields.SetField("Requested by NAME", Request.Form["RequestedByName"]);
                pdfFormFields.SetField("Requested by DEPT", Request.Form["RequestedByDepartment"]);

                pdfFormFields.SetField("AMOUNT", Request.Form["CashAmount"]);
                pdfFormFields.SetField("RECEIPT NO", Request.Form["CashReceipt"]);
                pdfFormFields.SetField("DATE RECEIVED", Request.Form["CashReceived"]);
                pdfFormFields.SetField("CHECK NO", Request.Form["CashCheckNo"]);
                pdfFormFields.SetField("VENDOR NO", Request.Form["CorpVenNo"]);

                // The form's checkboxes
                pdfFormFields.SetField("OUR PLANT", ourPlant.ToString());
                pdfFormFields.SetField("DESTINATION", destination.ToString());
                pdfFormFields.SetField("OTHER SPECIFY", otherSpecify.ToString());
                pdfFormFields.SetField("PREPAY CHARGE", charge.ToString());
                pdfFormFields.SetField("PREPAY ALLOW", allow.ToString());
                pdfFormFields.SetField("COLLECT", collect.ToString());

                if (cash)
                {
                    pdfFormFields.SetField("CASH SALE", cash.ToString());
                }

                if (credit)
                {
                    pdfFormFields.SetField("CREDIT SALE", credit.ToString());
                }
                if (creditMemo)
                {
                    pdfFormFields.SetField("CREDIT MEMO", creditMemo.ToString());
                }
                if (gatePass)
                {
                    pdfFormFields.SetField("GATE PASS", gatePass.ToString());
                }

                // The form's Items
                SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);

                int itemcount = sQLHelper.GetMsoItems(Convert.ToInt32(msoNumber)).Count();

                foreach (var item in sQLHelper.GetMsoItems(Convert.ToInt32(msoNumber)))
                {
                    pdfFormFields.SetField("CA STOCK Row" + itemcount, item.StockNumber);
                    pdfFormFields.SetField("DescriptionRow" + itemcount, item.Description);
                    pdfFormFields.SetField("QtyRow" + itemcount, item.Quantity.ToString());
                    pdfFormFields.SetField("UnitRow" + itemcount, item.Unit);
                    pdfFormFields.SetField("Gross WGTRow" + itemcount, item.Gross.ToString());
                    pdfFormFields.SetField("Tare WGTRow" + itemcount, item.Tare.ToString());
                    pdfFormFields.SetField("Net WGTRow" + itemcount, item.Net.ToString());
                    pdfFormFields.SetField("Mat WGTRow" + itemcount, item.ItemCount.ToString());

                    itemcount = itemcount - 1;
                }

                pdfFormFields.SetField("Remarks", Request.Form["Remarks"]);
                pdfFormFields.SetField("Shipping Instructions", Request.Form["ShippingInstructions"]);
                pdfFormFields.SetField("Shipment Completed By", Request.Form["CompletedBy"]);
                pdfFormFields.SetField("Buyer", Request.Form["Buyer"]);
                pdfFormFields.SetField("NAME OF VENDORS REPRESENTITIVE", Request.Form["returnRep"]);
                pdfFormFields.SetField("REPRESENTATIVE WHO AUTHORIZED RETURN", Request.Form["CenturyRepresentative"]);

                //// report by reading values from completed PDF
                //string sTmp = "Completed for " + pdfFormFields.GetField("f1_09(0)") + " " + pdfFormFields.GetField("f1_10(0)");
                // flatten the form to remove editting options, set it to false
                // to leave the form open to subsequent manual edits
                pdfStamper.FormFlattening = false;
                // close the pdf
                pdfStamper.Close();

                return RedirectToAction("Details", "Mso", new { @id = msoNumber });
            }
            catch (Exception ex)
            {
                string msoNumber = "";
                string[] number = Request.Form["RequestNumber"];

                number = number[0].Split(",");
                msoNumber = number[0].ToString();

                Log.Error(ex.Message);
                return RedirectToAction("Details", "Mso", new { @id = msoNumber });
            }
        }

        public void openPDF(string msoNumber)
        {
            Files print = new Files(_config["MsoSQLConnection"]);
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            Mso mso = new Mso();
            string FilePath = Directory.GetCurrentDirectory();
            string fileName = FilePath + "\\Printed\\" + msoNumber + ".pdf";
            var userName = User.Identity.Name;
            userName.ToString().Substring(8, userName.Length - 8);

            mso = sQLHelper.GetMso(Convert.ToInt32(msoNumber), userName);

            HttpContext.Response.ContentType = "application/pdf";
            HttpContext.Response.GetTypedHeaders();
            try
            {
                HttpContext.Response.SendFileAsync(print.CreatePdfFile(mso)).Wait();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                mso.Status = ex.Message;
            }

            return;
        }

        // POST: MsoController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);

            string msoNumber = "";
            string returnbool = "";
            string[] number = Request.Form["RequestNumber"];
            string[] returnstatus = Request.Form["Return"];

            number = number[0].Split(",");
            msoNumber = number[0].ToString();

            returnstatus = returnstatus[0].Split(",");
            returnbool = returnstatus[0].ToString();
            Mso mso = new Mso
            {
                RequestNumber = Convert.ToInt32(msoNumber.ToString()),
                RequestedByName = Request.Form["RequestedByName"],
                RequestedByDepartment = Request.Form["RequestedByDepartment"],
                Location = sQLHelper.GetPlant(User.Identity.Name),
                Fob = Convert.ToInt32(Request.Form["fob"]),
                PurchaseNumber = Request.Form["PurchaseNumber"],
                OrderNumber = Request.Form["OrderNumber"],
                WorkOrderNumber = Request.Form["WorkOrderNumber"],
                OtherReferenceNumber = Request.Form["OtherReferenceNumber"],
                BillOfLading = Request.Form["billOfLading"],
                Freight = Convert.ToInt32(Request.Form["freight"]),
                TransactionType = Request.Form["TransactionType"],
                CashAmount = Convert.ToInt32(Request.Form["cashAmount"]),
                CashCheckNo = Convert.ToInt32(Request.Form["cashCheckNo"]),
                CashReceived = Convert.ToInt32(Request.Form["cashReceived"]),
                CashReceipt = Convert.ToInt32(Request.Form["cashReceipt"]),
                CorpVenNo = Request.Form["CorpVenNo"].ToString(),
                Return = Convert.ToBoolean(returnbool),
                GoodsLoaned = Convert.ToBoolean(Request.Form["GoodsLoaned"]),
                GoodsShipped = Convert.ToBoolean(Request.Form["GoodsShipped"]),
                Other = Convert.ToBoolean(Request.Form["Other"]),
                GateVendor = Convert.ToBoolean(Request.Form["VendorRemove"]),
                Remarks = Request.Form["Remarks"].ToString(),
                ShippingInstructions = Request.Form["ShippingInstructions"],
                CompletedBy = Request.Form["CompletedBy"],
                Buyer = Request.Form["Buyer"],
                ReturnRep = Request.Form["returnRep"],
                CenturyRepresentative = Request.Form["CenturyRepresentative"],
                ShipDate = Convert.ToDateTime(Request.Form["ShipDate"]),
                ShipVia = Request.Form["ShipVia"]
            };
            try
            {
                sQLHelper.EditMso(mso);
                return RedirectToAction(nameof(Edit));
            }
            catch
            {
                return View(Request.Headers["Referer"].ToString());
            }
        }

        // GET: MsoController/Delete/5
        public ActionResult Delete(int msoNumber)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            sQLHelper.DeleteMsoById(msoNumber);
            return View();
        }

        // POST: MsoController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Search()
        {
            var searchItems = GetAllSearchItems();

            Search msoSearch = new Search();

            SearchOptions searchOptions = new SearchOptions
            {
                SearchItems = GetSelectListItems(searchItems)
            };

            msoSearch.SearchOption = searchOptions;

            msoSearch.MSOs = new List<Mso>();
            return View(msoSearch);
        }

        [HttpPost]
        public ActionResult Search(SearchOptions searchOption)

        {
            try
            {
                SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
                var searchItems = GetAllSearchItems();

                IList<Mso> msos = sQLHelper.SearchMsos(searchOption);

                searchOption.SearchItems = GetSelectListItems(searchItems);

                Search MsoSearch = new Search
                {
                    SearchOption = searchOption,
                    MSOs = msos
                };
                return View(MsoSearch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return View();
            }
        }

        private IEnumerable<string> GetAllSearchItems()
        {
            return new List<string>
            {
                "",
                "Requested By",
                "Creation Date",
                "Request Number",
                 "Item Name",
                 "Work Order",
                "Edited By",
                "Completed By",
                "Requested Department",
                "Ship To"
            };
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            // Create an empty list to hold result of the operation
            var selectList = new List<SelectListItem>();

            // For each string in the 'elements' variable, create a new SelectListItem object
            // that has both its Value and Text properties set to a particular value.
            // This will result in MVC rendering each item as:
            //     <option value="State Name">State Name</option>
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }

            return selectList;
        }
    }
}