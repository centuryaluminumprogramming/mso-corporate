﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MSO.DataAccess;
using MSO.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Serilog;

namespace MSO.API
{
    public class TemplateClass1
    {
        public TemplateClass1(IConfiguration config)
        {
            _config = config;
        }

        private readonly IConfiguration _config;

        [HttpGet]
        public Mso GetTemplate(string Name)
        {
            Log.Information("Made it to the API");
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            Mso mso = sQLHelper.GetTemplate(Name, "walkerm");

            return mso;
        }
    }
}