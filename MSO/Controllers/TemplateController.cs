﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MSO.DataAccess;
using MSO.Models;
using Serilog;
using System;
using System.Collections.Generic;
using X.PagedList;

namespace MSO.Controllers
{
    public class TemplateController : Controller
    {
        private string _MsoConnectionString;
        private readonly IConfiguration _config;

        public TemplateController(IConfiguration config)
        {
            _config = config;
        }

        // GET: ItemController
        public ActionResult Index(Mso mso)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            IList<Item> items = sQLHelper.GetMsoItems(mso.RequestNumber);

            return View(items);
        }

        // GET: ItemController/Edit/5
        public ActionResult Edit()
        {
            return View();
        }

        // POST: ItemController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ItemController/Delete/5
        public ActionResult Delete()
        {
            return View();
        }

        // POST: ItemController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult RemoveItem(int ItemCount, string RequestNumber)

        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            sQLHelper.RemoveItemFromTemplate(ItemCount, RequestNumber);

            return RedirectPermanentPreserveMethod(Request.Headers["Referer"].ToString());
        }

        public ActionResult Details(int id, string Name)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            Shipping shipping = new Shipping();
            Vendor vendor = new Vendor();
            Mso mso = sQLHelper.GetTemplate(Name, User.Identity.Name);

            mso.Items = sQLHelper.GetMsoTemplateItems(id);

            return RedirectToAction("Create", "Template", mso);
        }

        public ActionResult List(int? page, string sortOrder)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            Mso mso = new Mso();
            IList<Template> templates = sQLHelper.GetTemplates();

            int pageSize = 50;
            int pageNumber = (page ?? 1);

            pageNumber = (pageNumber != 0) ? pageNumber : 1;
            return View(templates.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult Create(Mso mso)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            string userName;
            userName = User.Identity.Name;
            userName = userName.ToString().Substring(8, userName.Length - 8);
            try
            {
                mso.RequestedByName = userName;
                mso.RequestNumber = sQLHelper.GetNextRequestNumber();

                if (mso.RoleId != 0)
                {
                    sQLHelper.CreateMso(mso);

                    mso.Items = sQLHelper.GetMsoItems(mso.RequestNumber);
                    return RedirectToAction("Create", "Mso", mso); //new { id = mso.RequestNumber });
                }
                else
                {
                    return RedirectToAction("Unauthorized", "Shared");
                }

                //return RedirectToAction("Create", "Mso", new { id = mso.RequestNumber });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return View();
            }
        }

        [HttpGet]
        public ActionResult GetTemplate(string Name, int id)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            string userName;
            userName = User.Identity.Name;
            userName = userName.ToString().Substring(8, userName.Length - 8);

            Mso mso = sQLHelper.GetTemplate(Name, userName);

            return RedirectToAction("Create", "Template", mso);
        }

        [HttpGet]
        public ActionResult GetTemplates(int? page, string sortOrder)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            IList<Template> templates = sQLHelper.GetTemplates();

            int pageSize = 50;
            int pageNumber = (page ?? 1);

            pageNumber = (pageNumber != 0) ? pageNumber : 1;
            return View(templates.ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        public ActionResult Create()
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            IList<Template> templates = sQLHelper.GetTemplates();
            return View(templates);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Template mso)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            string userName;
            userName = User.Identity.Name;
            userName = userName.ToString().Substring(8, userName.Length - 8);

            try
            {
                mso.RequestedByName = userName;
                mso.RequestNumber = sQLHelper.GetNextTemplateId();
                sQLHelper.CreateMsoTemplate(mso);
                return View(mso);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return View("Create", "Template");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddItem(int id, string StockNumber, string Description, double Quantity, string Unit, double Gross, double Tare, double Net, string Value, List<string> item)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            List<Item> items = new List<Item>(sQLHelper.GetMsoTemplateItems(id));

            try
            {
                Template template = new Template();
                int ItemCount = 0;

                if (items.Count == 0)
                {
                    ItemCount = 1;
                }
                else
                {
                    ItemCount = items.Count + 1;
                }

                sQLHelper.AddItemToTemplate(id, StockNumber, Description, Quantity, Unit, Gross, Tare, Net, Value, ItemCount);

                return RedirectToAction("Create", "Template", new { id = id });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return View("Create", "Template");
            }
        }
    }
}