﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using MSO.DataAccess;
using MSO.Models;
using Microsoft.Extensions.Configuration;
using X.PagedList;
using System.IO;
using Serilog;
using MSO.Tools;
using System;

namespace MSO.Controllers
{
    public class VendorController : Controller
    {
        private readonly IConfiguration _config;
        private readonly string _MsoConnectionString;

        public VendorController(IConfiguration config)
        {
            _config = config;
            if (string.IsNullOrEmpty(_MsoConnectionString))
            {
                _MsoConnectionString = _config["MsoSQLConnection"];
            }
        }

        // GET: VendorController
        public ActionResult Index(int? page)
        {
            Users utilities = new Users(_config["MsoSQLConnection"]);
            User user = new User();
            user = utilities.GetUserInfo(User.Identity.Name);

            if (string.IsNullOrEmpty(user.Plant))
            {
                return RedirectToAction("SelectPlant", "MSO");
            }
            else
            {
                SQLHelper sQLHelper = new SQLHelper(_MsoConnectionString);
                IList<Vendor> vendors = sQLHelper.GetVendors(User.Identity.Name);
                int pageSize = 30;
                int pageNumber = (page ?? 1);

                pageNumber = (pageNumber != 0) ? pageNumber : 1;
                return View(vendors.ToPagedList(pageNumber, pageSize));
            }
        }

        // GET: VendorController/Details/5
        public ActionResult Details()
        {
            return View();
        }

        // GET: VendorController/Create
        public ActionResult Create()
        {
            Users users = new Users(_config["MsoSQLConnection"]);
            User user = new User();

            user = users.GetUserInfo(User.Identity.Name);
            if (user.RoleId == 2)
            {
                SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);

                if (sQLHelper.GetUserAccess(User.Identity.Name) == 1)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }
            else
            {
                return RedirectToAction("UnauthorizedManager", "Shared");
            }
        }

        // POST: VendorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                Users users = new Users(_config["MsoSQLConnection"]);
                User user = new User();

                user = users.GetUserInfo(User.Identity.Name);

                if (user.RoleId == 2)
                {
                    SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
                    Vendor vendor = new Vendor
                    {
                        Name = Request.Form["Name"],
                        Address = Request.Form["Address"],
                        City = Request.Form["City"],
                        State = Request.Form["State"],
                        Zip = Request.Form["Zip"],
                        Plant = Request.Form["Plant"]
                    };

                    sQLHelper.CreateVendor(vendor);

                    return View("Create");
                }
                else
                {
                    return RedirectToAction("UnauthorizedManager", "Shared");
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex.Message);
                return View();
            }
        }

        // GET: VendorController/Edit/5
        public ActionResult Edit(int id)
        {
            Users users = new Users(_config["MsoSQLConnection"]);
            User user = new User();

            user = users.GetUserInfo(User.Identity.Name);
            if (user.RoleId == 2)
            {
                SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
                IEnumerable<Vendor> vendors = new List<Vendor>();

                if (sQLHelper.GetUserAccess(User.Identity.Name) == 1)
                {
                    return View(sQLHelper.GetVendorById(id));
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }
            else
            {
                return RedirectToAction("Unauthorized", "Shared");
            }
        }

        // POST: VendorController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: VendorController/Delete/5
        public ActionResult Delete(int Id)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            sQLHelper.DeleteVendorById(Id);
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Save(IFormCollection collection)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            string vendorId = Request.Form["Id"];

            Vendor vendor = new Vendor
            {
                Name = Request.Form["Name"],
                Address = Request.Form["Address"],
                City = Request.Form["City"],
                State = Request.Form["State"],
                Zip = Request.Form["Zip"],
                Plant = Request.Form["Plant"]
            };

            sQLHelper.UpdateVendor(vendor);

            return RedirectToAction("Edit", "Vendor", new { id = vendorId });
        }

        // POST: VendorController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch (System.Exception ex)
            {
                Log.Error(ex.Message);
                return View();
            }
        }

        public Vendor GetVendorByName(string Name)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);

            Vendor vendor = new Vendor();
            Name = (Name.Replace("&", ""));
            vendor = sQLHelper.GetVendorByName(Name);
            return vendor;
        }

        private static void CreateFileLoggerUsingJSONFile()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }
    }
}