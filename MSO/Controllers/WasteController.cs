﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;
using MSO.Models;
using PagedList;
using MSO.DataAccess;
using MSO.Tools;

namespace MSO.Controllers
{
    public class WasteController : Controller
    {
        private readonly IConfiguration _config;
        private string _MsoSQLConnection;

        public WasteController(IConfiguration config)
        {
            _config = config;
        }

        public ActionResult Index(string StartDate, string EndDate, int? page, string sortOrder)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            Utilities utilities = new Utilities(_config["MsoSQLConnection"]);
            IList<Waste> waste = new List<Waste>();

            if (page == null)
            {
                return View();
            }
            else
            {
                int pageSize = 50;
                int pageNumber = (page ?? 1);
                waste = sQLHelper.GetWastes();

                pageNumber = (pageNumber != 0) ? pageNumber : 1;
                return View(waste.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult Details(string id)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            Utilities utilities = new Utilities(_config["MsoSQLConnection"]);
            Waste waste = new Waste();
            waste = sQLHelper.GetWaste(id);
            return View(waste);
        }

        public ActionResult Waste(string StartDate, string EndDate, int? page, string sortOrder)
        {
            Files files = new Files(_config["MsoSQLConnection"]);
            IList<Waste> waste = new List<Waste>();

            if (StartDate == null && EndDate == null)
            {
                return View();
            }
            else
            {
                int pageSize = 50;
                int pageNumber = (page ?? 1);
                waste = files.BuildWasteFile(StartDate, EndDate);

                pageNumber = (pageNumber != 0) ? pageNumber : 1;
                return View(waste.ToPagedList(pageNumber, pageSize));
            }
        }

        //public void DownloadReport(string StartDate, string EndDate, string user)
        //{
        //    Utilities utilities = new Utilities(_config);
        //    IList<Waste> waste = new List<Waste>();
        //    user = User.Identity.Name;
        //    waste = utilities.DownloadWasteExcel(StartDate, EndDate, user);
        //    return;
        //}
    }
}