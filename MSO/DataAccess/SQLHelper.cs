﻿using MSO.Models;
using MSO.Tools;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSO.DataAccess
{
    public class SQLHelper
    {
        private static string _MsoConnectionString;
        private static readonly StringBuilder sb = new StringBuilder();
        public String[] city;
        private String[] state;

        public SQLHelper(string MsoSQLConnection)
        {
            _MsoConnectionString = MsoSQLConnection;
        }

        public void AddItemToMso(int RequestNumber, string StockNumber, string Description, double Quantity, string Unit, double Gross, double Tare, double Net, string Value, int ItemCount)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("AddItem", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("reqNum", SqlDbType.Int).Value = RequestNumber;
                    cmd.Parameters.Add("matStockItem", SqlDbType.VarChar).Value = StockNumber;
                    cmd.Parameters.Add("matDesc", SqlDbType.VarChar).Value = Description;
                    cmd.Parameters.Add("matQty", SqlDbType.BigInt).Value = Quantity;
                    cmd.Parameters.Add("matUnit", SqlDbType.VarChar).Value = Unit;
                    cmd.Parameters.Add("matGross", SqlDbType.BigInt).Value = Gross;
                    cmd.Parameters.Add("matTare", SqlDbType.BigInt).Value = Tare;
                    cmd.Parameters.Add("matNet", SqlDbType.BigInt).Value = Net;
                    cmd.Parameters.Add("matValue", SqlDbType.VarChar).Value = Value;
                    cmd.Parameters.Add("matItem", SqlDbType.Int).Value = ItemCount;
                    cmd.ExecuteNonQuery();
                }
                return;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public void RemoveItemFromMso(int ItemCount, string RequestNumber)
        {
            try
            {
                if (RequestNumber != null)
                {
                    using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("RemoveItem", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("reqNum", SqlDbType.Int).Value = RequestNumber;
                        cmd.Parameters.Add("matItem", SqlDbType.VarChar).Value = ItemCount;
                        cmd.ExecuteNonQuery();
                    }
                    return;
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public void AddItemToTemplate(int RequestNumber, string StockNumber, string Description, double Quantity, string Unit, double Gross, double Tare, double Net, string Value, int ItemCount)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("AddItem", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("reqNum", SqlDbType.Int).Value = RequestNumber;
                    cmd.Parameters.Add("matStockItem", SqlDbType.VarChar).Value = StockNumber;
                    cmd.Parameters.Add("matDesc", SqlDbType.VarChar).Value = Description;
                    cmd.Parameters.Add("matQty", SqlDbType.BigInt).Value = Quantity;
                    cmd.Parameters.Add("matUnit", SqlDbType.VarChar).Value = Unit;
                    cmd.Parameters.Add("matGross", SqlDbType.BigInt).Value = Gross;
                    cmd.Parameters.Add("matTare", SqlDbType.BigInt).Value = Tare;
                    cmd.Parameters.Add("matNet", SqlDbType.BigInt).Value = Net;
                    cmd.Parameters.Add("matValue", SqlDbType.VarChar).Value = Value;
                    cmd.Parameters.Add("matItem", SqlDbType.Int).Value = ItemCount;
                    cmd.ExecuteNonQuery();
                }
                return;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public void RemoveItemFromTemplate(int ItemCount, string RequestNumber)
        {
            try
            {
                if (RequestNumber != null)
                {
                    using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("RemoveItem", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("reqNum", SqlDbType.Int).Value = RequestNumber;
                        cmd.Parameters.Add("matItem", SqlDbType.VarChar).Value = ItemCount;
                        cmd.ExecuteNonQuery();
                    }
                    return;
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public int GetNextRequestNumber()
        {
            int rqNum = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetNextMso", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader rdr = cmd.ExecuteReader())

                    {
                        while (rdr.Read())
                        {
                            Mso mso = new Mso();
                            rqNum = Convert.ToInt32(rdr["reqnum"].ToString());
                        }
                    }

                    return rqNum;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return 0;
            }
        }

        public int GetNextTemplateId()
        {
            int rqNum = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetNextTemplate", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader rdr = cmd.ExecuteReader())

                    {
                        while (rdr.Read())
                        {
                            Mso mso = new Mso();
                            rqNum = Convert.ToInt32(rdr["reqnum"].ToString());
                        }
                    }

                    return rqNum;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return 0;
            }
        }

        public IList<Mso> SearchMsos(SearchOptions searchOption)
        {
            IList<Mso> msos = new List<Mso>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SearchMso", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //if (!string.IsNullOrEmpty(searchOption.SearchType))
                    //{
                    //    var test = "Ya";
                    //}

                    switch (searchOption.SearchType)
                    {
                        case "Requested By":
                            cmd.Parameters.Add("req_by", SqlDbType.VarChar).Value = searchOption.SearchString;
                            break;

                        case "Creation Date":
                            cmd.Parameters.Add("startDate", SqlDbType.Date).Value = searchOption.startDate;
                            cmd.Parameters.Add("endDate", SqlDbType.Date).Value = searchOption.endDate;
                            break;

                        case "Item Name":
                            cmd.Parameters.Add("matItem", SqlDbType.VarChar).Value = searchOption.SearchString;
                            break;

                        case "Work Order":
                            cmd.Parameters.Add("woNum", SqlDbType.Int).Value = searchOption.SearchString;
                            break;

                        case "Edited By":
                            cmd.Parameters.Add("lastChangeUser", SqlDbType.VarChar).Value = searchOption.SearchString;
                            break;

                        case "Completed By":
                            cmd.Parameters.Add("appCompBy", SqlDbType.VarChar).Value = searchOption.SearchString;
                            break;

                        case "Requested Department":
                            cmd.Parameters.Add("appReqDept", SqlDbType.VarChar).Value = searchOption.SearchString;
                            break;

                        case "Ship To":
                            cmd.Parameters.Add("ShipTo", SqlDbType.VarChar).Value = searchOption.SearchString;
                            break;

                        case "Request Number":
                            cmd.Parameters.Add("msoNumber", SqlDbType.VarChar).Value = searchOption.SearchString;
                            break;

                        default:
                            break;
                    }

                    Log.Information(cmd.Parameters.Count.ToString());

                    using (SqlDataReader rdr = cmd.ExecuteReader())

                    {
                        while (rdr.Read())
                        {
                            Mso mso = new Mso();
                            mso.RequestNumber = Convert.ToInt32(rdr["reqNum"].ToString());
                            mso.Vendor = GetVendorByRequest(Convert.ToInt32(rdr["reqNum"].ToString()));

                            if (searchOption.SearchType != "Item Name")
                            {
                                mso.CreationDate = (rdr["msoCreationDate"].ToString() != null) ? Convert.ToDateTime(rdr["msoCreationDate"]) : Convert.ToDateTime(rdr["msoShipDate"]);
                            }
                            else
                            {
                                mso.CreationDate = (String.IsNullOrEmpty(rdr["msoCreationDate"].ToString())) ? Convert.ToDateTime("01/01/4041") : Convert.ToDateTime(rdr["msoCreationDate"]);
                            }

                            msos.Add(mso);
                        }
                    }
                }
                msos = (IList<Mso>)msos.Reverse();

                return msos;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return msos;
            }
        }

        public void CreateMsoTemplate(Template mso)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateMso", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("id", SqlDbType.Int).Value = mso.RequestNumber;
                    cmd.Parameters.Add("req_by", SqlDbType.VarChar).Value = mso.RequestedByName;
                    cmd.Parameters.Add("Location", SqlDbType.VarChar).Value = GetPlant(mso.RequestedByName);
                    cmd.Parameters.Add("msoCreationDate", SqlDbType.DateTime).Value = DateTime.Now;

                    cmd.Parameters.Add("ShipVia", SqlDbType.VarChar).Value = mso.Shipment.ShipVia;
                    cmd.Parameters.Add("ShipTo1", SqlDbType.VarChar).Value = mso.Shipment.Name;
                    cmd.Parameters.Add("ShipTo2", SqlDbType.VarChar).Value = mso.Shipment.Address;
                    cmd.Parameters.Add("ShipTo3", SqlDbType.VarChar).Value = mso.Shipment.City + ", " + mso.Shipment.State + " " + mso.Shipment.Zip;
                    cmd.Parameters.Add("ShipTo4", SqlDbType.VarChar).Value = mso.Shipment.Attention;

                    cmd.Parameters.Add("SoldTo1", SqlDbType.VarChar).Value = mso.Vendor.Name;
                    cmd.Parameters.Add("SoldTo2", SqlDbType.VarChar).Value = mso.Vendor.Address;
                    cmd.Parameters.Add("SoldTo3", SqlDbType.VarChar).Value = mso.Vendor.City + ", " + mso.Vendor.State + " " + mso.Vendor.Zip;
                    cmd.Parameters.Add("SoldTo4", SqlDbType.VarChar).Value = mso.Vendor.Attention;

                    cmd.Parameters.Add("fob", SqlDbType.VarChar).Value = mso.Fob;
                    cmd.Parameters.Add("purchReqNum", SqlDbType.VarChar).Value = mso.PurchaseNumber;
                    cmd.Parameters.Add("purchOrder", SqlDbType.Int).Value = Convert.ToInt32(mso.OrderNumber);
                    cmd.Parameters.Add("woNum", SqlDbType.VarChar).Value = mso.WorkOrderNumber;
                    cmd.Parameters.Add("otherRefNum", SqlDbType.VarChar).Value = mso.OtherReferenceNumber;
                    cmd.Parameters.Add("billOfLading", SqlDbType.VarChar).Value = mso.BillOfLading;
                    cmd.Parameters.Add("freight", SqlDbType.Int).Value = Convert.ToInt32(mso.Freight);
                    cmd.Parameters.Add("tran_Type", SqlDbType.VarChar).Value = mso.TransactionType;
                    cmd.Parameters.Add("cashAmount", SqlDbType.VarChar).Value = mso.CashAmount;
                    cmd.Parameters.Add("cashCheckNo", SqlDbType.VarChar).Value = mso.CashCheckNo;
                    cmd.Parameters.Add("cashReceived", SqlDbType.VarChar).Value = (mso.CashReceived.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("cashReceipt", SqlDbType.VarChar).Value = (mso.CashReceipt.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("corpVenNo", SqlDbType.VarChar).Value = mso.CorpVenNo;
                    cmd.Parameters.Add("gateReturn", SqlDbType.VarChar).Value = (mso.Return.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateGoodsEquip", SqlDbType.VarChar).Value = (mso.GoodsLoaned.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateGoods", SqlDbType.VarChar).Value = (mso.GoodsShipped.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateOther", SqlDbType.VarChar).Value = (mso.Other.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateVendor", SqlDbType.VarChar).Value = (mso.GateVendor.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("userRemarks", SqlDbType.VarChar).Value = mso.Remarks;
                    cmd.Parameters.Add("userInstructions", SqlDbType.VarChar).Value = mso.ShippingInstructions;
                    cmd.Parameters.Add("appReqDept", SqlDbType.VarChar).Value = mso.RequestedByDepartment;
                    cmd.Parameters.Add("appCompBy", SqlDbType.VarChar).Value = mso.CompletedBy;
                    cmd.Parameters.Add("appReqName", SqlDbType.VarChar).Value = mso.RequestedByName;
                    cmd.Parameters.Add("buyerSelect", SqlDbType.VarChar).Value = mso.Buyer;
                    cmd.Parameters.Add("returnRep", SqlDbType.VarChar).Value = mso.ReturnRep;
                    cmd.Parameters.Add("cenRep", SqlDbType.VarChar).Value = mso.CenturyRepresentative;
                    cmd.Parameters.Add("msoShipDate", SqlDbType.VarChar).Value = mso.ShipDate;
                    cmd.Parameters.Add("VendorId", SqlDbType.VarChar).Value = mso.Vendor.Id;

                    cmd.ExecuteNonQuery();
                }

                return;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public void CreateMso(Mso mso)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateMso", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("reqNum", SqlDbType.Int).Value = mso.RequestNumber;
                    cmd.Parameters.Add("req_by", SqlDbType.VarChar).Value = mso.RequestedByName;
                    cmd.Parameters.Add("Location", SqlDbType.VarChar).Value = GetPlant(mso.RequestedByName);
                    cmd.Parameters.Add("msoCreationDate", SqlDbType.DateTime).Value = DateTime.Now;

                    cmd.Parameters.Add("ShipVia", SqlDbType.VarChar).Value = mso.Shipment.ShipVia;
                    cmd.Parameters.Add("ShipTo1", SqlDbType.VarChar).Value = mso.Shipment.Name;
                    cmd.Parameters.Add("ShipTo2", SqlDbType.VarChar).Value = mso.Shipment.Address;
                    cmd.Parameters.Add("ShipTo3", SqlDbType.VarChar).Value = mso.Shipment.City + ", " + mso.Shipment.State + " " + mso.Shipment.Zip;
                    cmd.Parameters.Add("ShipTo4", SqlDbType.VarChar).Value = mso.Shipment.Attention;

                    cmd.Parameters.Add("SoldTo1", SqlDbType.VarChar).Value = mso.SoldTo1;
                    cmd.Parameters.Add("SoldTo2", SqlDbType.VarChar).Value = mso.SoldTo2;
                    cmd.Parameters.Add("SoldTo3", SqlDbType.VarChar).Value = mso.SoldTo3 + ", " + mso.SoldTo4 + " " + mso.Vendor.Zip;
                    //if (mso.SoldTo4.Contains("Attn"))
                    //{
                    //    cmd.Parameters.Add("SoldTo4", SqlDbType.VarChar).Value = mso.Vendor.Attention;
                    //}
                    if (mso.SoldTo4 != null)
                    {
                        cmd.Parameters.Add("SoldTo4", SqlDbType.VarChar).Value = mso.Vendor.Attention;
                    }

                    cmd.Parameters.Add("fob", SqlDbType.VarChar).Value = mso.Fob;
                    cmd.Parameters.Add("purchReqNum", SqlDbType.VarChar).Value = mso.PurchaseNumber;
                    cmd.Parameters.Add("purchOrder", SqlDbType.Int).Value = Convert.ToInt32(mso.OrderNumber);
                    cmd.Parameters.Add("woNum", SqlDbType.VarChar).Value = mso.WorkOrderNumber;
                    cmd.Parameters.Add("otherRefNum", SqlDbType.VarChar).Value = mso.OtherReferenceNumber;
                    cmd.Parameters.Add("billOfLading", SqlDbType.VarChar).Value = mso.BillOfLading;
                    cmd.Parameters.Add("freight", SqlDbType.Int).Value = Convert.ToInt32(mso.Freight);
                    cmd.Parameters.Add("tran_Type", SqlDbType.VarChar).Value = mso.TransactionType;
                    cmd.Parameters.Add("cashAmount", SqlDbType.VarChar).Value = mso.CashAmount;
                    cmd.Parameters.Add("cashCheckNo", SqlDbType.VarChar).Value = mso.CashCheckNo;
                    cmd.Parameters.Add("cashReceived", SqlDbType.VarChar).Value = (mso.CashReceived.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("cashReceipt", SqlDbType.VarChar).Value = (mso.CashReceipt.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("corpVenNo", SqlDbType.VarChar).Value = mso.CorpVenNo;
                    cmd.Parameters.Add("gateReturn", SqlDbType.VarChar).Value = (mso.Return.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateGoodsEquip", SqlDbType.VarChar).Value = (mso.GoodsLoaned.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateGoods", SqlDbType.VarChar).Value = (mso.GoodsShipped.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateOther", SqlDbType.VarChar).Value = (mso.Other.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateVendor", SqlDbType.VarChar).Value = (mso.GateVendor.ToString() == "True") ? 1 : 0;
                    //cmd.Parameters.Add("gateRepair", SqlDbType.VarChar).Value = (mso.Repaired.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("userRemarks", SqlDbType.VarChar).Value = mso.Remarks;
                    cmd.Parameters.Add("userInstructions", SqlDbType.VarChar).Value = mso.ShippingInstructions;
                    cmd.Parameters.Add("appReqDept", SqlDbType.VarChar).Value = mso.RequestedByDepartment;
                    cmd.Parameters.Add("appCompBy", SqlDbType.VarChar).Value = mso.CompletedBy;
                    cmd.Parameters.Add("appReqName", SqlDbType.VarChar).Value = mso.RequestedByName;
                    cmd.Parameters.Add("buyerSelect", SqlDbType.VarChar).Value = mso.Buyer;
                    cmd.Parameters.Add("returnRep", SqlDbType.VarChar).Value = mso.ReturnRep;
                    cmd.Parameters.Add("cenRep", SqlDbType.VarChar).Value = mso.CenturyRepresentative;
                    cmd.Parameters.Add("msoShipDate", SqlDbType.VarChar).Value = mso.ShipDate;
                    cmd.Parameters.Add("VendorId", SqlDbType.VarChar).Value = mso.Vendor.Id;

                    cmd.ExecuteNonQuery();
                }

                return;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public void EditMso(Mso mso)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateMso", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("reqNum", SqlDbType.Int).Value = mso.RequestNumber;
                    cmd.Parameters.Add("shipViaTxt", SqlDbType.VarChar).Value = mso.ShipVia;
                    cmd.Parameters.Add("req_by", SqlDbType.VarChar).Value = mso.RequestedByName;
                    cmd.Parameters.Add("Location", SqlDbType.VarChar).Value = GetPlant(mso.RequestedByName);
                    cmd.Parameters.Add("fob", SqlDbType.VarChar).Value = mso.Fob;
                    cmd.Parameters.Add("purchReqNum", SqlDbType.VarChar).Value = mso.PurchaseNumber;
                    cmd.Parameters.Add("purchOrder", SqlDbType.Int).Value = Convert.ToInt32(mso.OrderNumber);
                    cmd.Parameters.Add("woNum", SqlDbType.VarChar).Value = mso.WorkOrderNumber;
                    cmd.Parameters.Add("otherRefNum", SqlDbType.VarChar).Value = mso.OtherReferenceNumber;
                    cmd.Parameters.Add("billOfLading", SqlDbType.VarChar).Value = mso.BillOfLading;
                    cmd.Parameters.Add("freight", SqlDbType.Int).Value = Convert.ToInt32(mso.Freight);
                    cmd.Parameters.Add("tran_Type", SqlDbType.VarChar).Value = mso.TransactionType;
                    cmd.Parameters.Add("cashAmount", SqlDbType.VarChar).Value = mso.CashAmount;
                    cmd.Parameters.Add("cashCheckNo", SqlDbType.VarChar).Value = mso.CashCheckNo;
                    cmd.Parameters.Add("cashReceived", SqlDbType.VarChar).Value = (mso.CashReceived.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("cashReceipt", SqlDbType.VarChar).Value = (mso.CashReceipt.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("corpVenNo", SqlDbType.VarChar).Value = mso.CorpVenNo;
                    cmd.Parameters.Add("gateEquip", SqlDbType.VarChar).Value = (mso.GateEquip.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateReturn", SqlDbType.VarChar).Value = (mso.Return.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateGoodsEquip", SqlDbType.VarChar).Value = (mso.GoodsLoaned.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateGoods", SqlDbType.VarChar).Value = (mso.GoodsShipped.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateOther", SqlDbType.VarChar).Value = (mso.Other.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("gateVendor", SqlDbType.VarChar).Value = (mso.GateVendor.ToString() == "True") ? 1 : 0;
                    cmd.Parameters.Add("userRemarks", SqlDbType.VarChar).Value = mso.Remarks;
                    cmd.Parameters.Add("userInstructions", SqlDbType.VarChar).Value = mso.ShippingInstructions;
                    cmd.Parameters.Add("appReqDept", SqlDbType.VarChar).Value = mso.RequestedByDepartment;
                    cmd.Parameters.Add("appCompBy", SqlDbType.VarChar).Value = mso.CompletedBy;
                    cmd.Parameters.Add("appReqName", SqlDbType.VarChar).Value = mso.RequestedByName;
                    cmd.Parameters.Add("buyerSelect", SqlDbType.VarChar).Value = mso.Buyer;
                    cmd.Parameters.Add("returnRep", SqlDbType.VarChar).Value = mso.ReturnRep;
                    cmd.Parameters.Add("cenRep", SqlDbType.VarChar).Value = mso.CenturyRepresentative;
                    cmd.Parameters.Add("msoShipDate", SqlDbType.VarChar).Value = mso.ShipDate;

                    cmd.ExecuteNonQuery();
                }

                return;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public int GetUserAccess(string UserName)
        {
            DataSet ds = new DataSet("ApplicationAccess");
            int RoleId = 0;
            UserName = UserName.ToString().Substring(8, UserName.Length - 8);
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand sqlComm = new SqlCommand("getApplicationAccess", conn);
                    sqlComm.Parameters.AddWithValue("UserName", UserName);
                    sqlComm.Parameters.AddWithValue("ApplicationID", 6);

                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;

                    da.Fill(ds);

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        RoleId = Convert.ToInt32(row["RoleId"].ToString());
                    }
                    return RoleId;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return 0;
            }
        }

        public string UpdatePlant(string userName, string GUID, string plant)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdatePlant", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("UserName", SqlDbType.VarChar).Value = userName;
                    cmd.Parameters.Add("GUID", SqlDbType.VarChar).Value = GUID;
                    cmd.Parameters.Add("Plant", SqlDbType.VarChar).Value = plant;

                    cmd.ExecuteNonQuery();
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public string InsertUserPlant(string userName, string GUID, string Plant, int RoleId)
        {
            using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("UserPlant", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("UserName", SqlDbType.VarChar).Value = userName;
                cmd.Parameters.Add("GUID", SqlDbType.VarChar).Value = GUID;
                cmd.Parameters.Add("Plant", SqlDbType.VarChar).Value = Plant;
                cmd.Parameters.Add("RoleId", SqlDbType.Int).Value = RoleId;

                cmd.ExecuteNonQuery();
            }
            return null;
        }

        public IList<Plant> GetPlants()
        {
            IList<Plant> plants = new List<Plant>();

            using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetPlants", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader rdr = cmd.ExecuteReader())

                {
                    while (rdr.Read())
                    {
                        Plant plant = new Plant();
                        plant.Id = Convert.ToInt32(rdr["Id"].ToString());
                        plant.Name = rdr["Location"].ToString();

                        plants.Add(plant);
                    }
                }
            }
            return plants;
        }

        public string GetPlant(string userName)
        {
            DataSet ds = new DataSet("Plant");
            string userGUID;

            try
            {
                if (!userName.Equals(null))
                {
                    using (var context = new PrincipalContext(ContextType.Domain, "Century"))
                    {
                        UserPrincipal User = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);

                        userGUID = Convert.ToString(User.Guid);
                        Log.Information("Found user " + userName + " with the GUID: " + userGUID);
                    }
                }
                else
                {
                    using (var context = new PrincipalContext(ContextType.Domain, "Century"))
                    {
                        UserPrincipal User = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, "");

                        userGUID = Convert.ToString(User.Guid);

                        using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                        {
                            SqlCommand sqlComm = new SqlCommand("UpdatePlant", conn);
                            sqlComm.Parameters.AddWithValue("userGUID", userGUID);
                            sqlComm.Parameters.AddWithValue("username", userName);

                            sqlComm.CommandType = CommandType.StoredProcedure;

                            sqlComm.ExecuteNonQuery();
                        }
                        Log.Information("Found user " + userName + " with the GUID: " + userGUID);
                    }
                }
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand sqlComm = new SqlCommand("getPlantbyGUID", conn);
                    sqlComm.Parameters.AddWithValue("userGUID", userGUID);

                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;

                    da.Fill(ds);

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Log.Information(GetLocationFromID(row[0].ToString().ToLowerInvariant()));
                        return GetLocationFromID(row[0].ToString().ToLowerInvariant());
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public int GetRoleId(string userName)
        {
            Users utilities = new Users(_MsoConnectionString);
            User user = new User();

            user = utilities.GetUserInfo(userName);
            return user.RoleId;
        }

        public IList<Mso> GetMsos(string userName)
        {
            IList<Mso> msos = new List<Mso>();
            Users utilities = new Users(_MsoConnectionString);
            User user = new User();

            user = utilities.GetUserInfo(userName);
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "GetAllMsos";
                    cmd.Parameters.Add("@Plant", SqlDbType.VarChar).Value = user.Plant;
                    conn.Open();

                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                    sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDat.SelectCommand = cmd;
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Mso mso = new Mso
                        {
                            RequestNumber = Convert.ToInt32(rdr["reqnum"].ToString()),
                            OrderNumber = (!String.IsNullOrEmpty(rdr["purchOrder"].ToString())) ? rdr["purchOrder"].ToString() : null,
                            RoleId = user.RoleId,
                            ShipDate = Convert.ToDateTime(rdr["msoShipDate"].ToString())
                        };
                        //var testdate = rdr["msoShipDate"].ToString();

                        msos.Add(mso);
                    }
                    msos = (IList<Mso>)msos.Reverse();
                    IEnumerable<Mso> sortedEnum = msos.OrderBy(f => f.RequestNumber);
                    IList<Mso> sortedMso = sortedEnum.ToList();
                    return sortedMso;
                    //return msos;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return msos;
            }
        }

        public class SpecialComparer : IComparer<decimal>
        {
            /// <summary>
            /// Compare two decimal numbers by their fractional parts.
            /// </summary>
            /// <param name="d1">The first decimal to compare.</param>
            /// <param name="d2">The second decimal to compare.</param>
            /// <returns>1 if the first decimal's fractional part
            /// is greater than the second decimal's fractional part,
            /// -1 if the first decimal's fractional
            /// part is less than the second decimal's fractional part,
            /// or the result of calling Decimal.Compare()
            /// if the fractional parts are equal.</returns>
            public int Compare(decimal d1, decimal d2)
            {
                decimal requestNumber1, requestNumber2;

                // Get the fractional part of the first number.
                try
                {
                    requestNumber1 = decimal.Remainder(d1, decimal.Floor(d1));
                }
                catch (DivideByZeroException)
                {
                    requestNumber1 = d1;
                }
                // Get the fractional part of the second number.
                try
                {
                    requestNumber2 = decimal.Remainder(d2, decimal.Floor(d2));
                }
                catch (DivideByZeroException)
                {
                    requestNumber2 = d2;
                }

                if (requestNumber1 == requestNumber2)
                    return Decimal.Compare(d1, d2);
                else if (requestNumber1 > requestNumber2)
                    return 1;
                else
                    return -1;
            }
        }

        public Mso GetMso(int requestNumber, string userName)
        {
            Mso mso = new Mso();
            Users utilities = new Users(_MsoConnectionString);
            User user = new User();

            user = utilities.GetUserInfo(userName);
            int cashAmount = 0;
            int cashReceipt = 0;
            int checkNo = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetMso", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@reqNum", SqlDbType.Int).Value = requestNumber;
                    using (SqlDataReader rdr = cmd.ExecuteReader())

                    {
                        while (rdr.Read())
                        {
                            mso.RequestNumber = Convert.ToInt32(rdr["reqnum"].ToString());
                            mso.PurchaseNumber = (!string.IsNullOrEmpty(rdr["purchReqNum"].ToString())) ? rdr["purchReqNum"].ToString() : null;
                            mso.OrderNumber = (!string.IsNullOrEmpty(rdr["purchOrder"].ToString())) ? rdr["purchOrder"].ToString() : null;
                            mso.OtherReferenceNumber = (!string.IsNullOrEmpty(rdr["otherRefNum"].ToString())) ? rdr["otherRefNum"].ToString() : null;
                            mso.WorkOrderNumber = (!string.IsNullOrEmpty(rdr["woNum"].ToString())) ? rdr["woNum"].ToString() : null;
                            mso.Vendor = (!String.IsNullOrEmpty(rdr["VendorId"].ToString())) ? GetVendorById(Convert.ToInt32(rdr["VendorId"])) : GetVendorByName(rdr["ShipTo1"].ToString());

                            mso.SoldTo1 = rdr["Soldto1"].ToString();
                            mso.SoldTo2 = rdr["Soldto2"].ToString();
                            mso.SoldTo3 = rdr["Soldto3"].ToString();
                            mso.SoldTo4 = rdr["Soldto4"].ToString();
                            mso.Buyer = rdr["buyerSelect"].ToString();

                            mso.RequestedByDepartment = rdr["appReqDept"].ToString();
                            mso.RequestedByName = rdr["appReqName"].ToString();
                            mso.CompletedBy = rdr["appCompBy"].ToString();
                            mso.Remarks = rdr["userRemarks"].ToString();
                            mso.ShippingInstructions = rdr["userInstructions"].ToString();
                            mso.CompletedBy = rdr["lastChangeUser"].ToString();
                            mso.CenturyRepresentative = rdr["cenRep"].ToString();
                            mso.VendorsRepresentative = rdr["returnRep"].ToString();

                            if (!String.IsNullOrEmpty(rdr["ShipTo3"].ToString()) && rdr["ShipTo3"].ToString().Contains(","))
                            {
                                var cityinfo = rdr["ShipTo3"].ToString();

                                String[] city = cityinfo.Split(",");

                                var stateinfo = city[1].ToString();
                                String[] state = stateinfo.Split(" ");

                                mso.Vendor.City = city[0].ToString();
                                mso.Vendor.State = state[1].ToString();
                                var zipcode = (state.Count() == 3) ? state[2].ToString() : null;
                                mso.Vendor.Zip = zipcode;
                            }
                            else
                            {
                                var cityinfo = rdr["ShipTo3"].ToString();
                                if (!string.IsNullOrEmpty(cityinfo))
                                {
                                    String[] city = cityinfo.Split(" ");

                                    mso.Vendor.City = city[0].ToString();
                                    mso.Vendor.State = city[1].ToString();
                                    var zipcode = (city.Count()! > 3) ? city[2].ToString() : null;
                                    mso.Vendor.Zip = zipcode;
                                }
                            }

                            mso.Vendor.Name = rdr["ShipTo1"].ToString();
                            mso.Vendor.Address = rdr["ShipTo2"].ToString();
                            mso.GoodsShipped = (rdr["gateGoods"].ToString() == "on") ? true : false;
                            mso.GoodsLoaned = (rdr["gateGoodsEquip"].ToString() == "on") ? true : false;
                            mso.GateVendor = (rdr["gateVendor"].ToString() == "on") ? true : false;
                            mso.Other = (rdr["gateOther"].ToString() == "on") ? true : false;
                            mso.Fob = (!string.IsNullOrEmpty(rdr["fob"].ToString())) ? Convert.ToInt32(rdr["fob"].ToString()) : 0;
                            mso.CreationDate = (!string.IsNullOrEmpty(rdr["msoCreationDate"].ToString())) ? Convert.ToDateTime(rdr["msoCreationDate"].ToString()) : Convert.ToDateTime("1/1/00 00:00:00");
                            mso.ShipDate = (!string.IsNullOrEmpty(rdr["msoShipDate"].ToString())) ? Convert.ToDateTime(rdr["msoShipDate"].ToString()) : Convert.ToDateTime("1/1/00 00:00:00");
                            mso.Return = (!string.IsNullOrWhiteSpace(rdr["gateReturn"].ToString()) && rdr["gateReturn"].ToString() != " ") ? true : false;
                            mso.Repaired = (!string.IsNullOrWhiteSpace(rdr["gateEquip"].ToString()) && rdr["gateEquip"].ToString() != " ") ? true : false;
                            mso.Freight = (!string.IsNullOrEmpty(rdr["freight"].ToString())) ? Convert.ToInt32(rdr["freight"].ToString()) : 0;

                            if (!string.IsNullOrEmpty(rdr["fob"].ToString()))
                            {
                                switch (Convert.ToInt32(rdr["fob"].ToString()))
                                {
                                    case 1:
                                        GetVendorByRequest(mso.RequestNumber);

                                        break;

                                    case 2:
                                        mso.Destination = rdr["fob"].ToString();

                                        break;

                                    case 3:
                                        mso.OtherSpecify = rdr["fob"].ToString();
                                        break;
                                }
                            }

                            cashAmount = (!string.IsNullOrEmpty(rdr["cashAmount"].ToString())) ? Convert.ToInt32(rdr["cashAmount"].ToString()) : 0;
                            cashReceipt = (!string.IsNullOrEmpty(rdr["cashReceipt"].ToString())) ? Convert.ToInt32(rdr["cashReceipt"].ToString()) : 0;
                            checkNo = (!string.IsNullOrEmpty(rdr["cashCheckNo"].ToString())) ? Convert.ToInt32(rdr["cashCheckNo"].ToString()) : 0;

                            mso.CashAmount = cashAmount;
                            mso.CashReceipt = cashReceipt;
                            mso.CashCheckNo = checkNo;
                            mso.CorpVenNo = rdr["corpVenNo"].ToString();
                            mso.TransactionType = rdr["tran_type"].ToString();
                            mso.ShipVia = rdr["shipviaTxt"].ToString();
                            mso.ShippingInstructions = rdr["userInstructions"].ToString();
                            mso.RequestedByName = rdr["appReqName"].ToString();
                            mso.RequestedByDepartment = rdr["appReqDept"].ToString();
                            mso.ReturnRep = rdr["returnRep"].ToString();
                            mso.CompletedBy = rdr["appCompBy"].ToString();
                            mso.Location = rdr["Location"].ToString();
                            mso.BillOfLading = (!String.IsNullOrEmpty(rdr["billOfLading"].ToString())) ? rdr["billOfLading"].ToString() : null;
                            mso.RoleId = user.RoleId;
                        }
                    }

                    return mso;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return mso;
            }
        }

        public Vendor GetVendorByRequest(int requestNumber)
        {
            using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "GetVendorByRequest";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@reqNum", SqlDbType.VarChar).Value = requestNumber;
                cmd.Connection = conn;
                conn.Open();
                SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDat.SelectCommand = cmd;
                SqlDataReader rdr = cmd.ExecuteReader();

                Vendor vendor = new Vendor();

                try
                {
                    while (rdr.Read())
                    {
                        if (rdr["Name"].ToString() != "")
                        {
                            vendor.Name = rdr["Name"].ToString();//.Replace(".", "").ToString();
                            vendor.Address = rdr["Address"].ToString();
                            vendor.City = rdr["City"].ToString();
                            vendor.State = rdr["State"].ToString();
                            vendor.Zip = rdr["Zip"].ToString();
                            vendor.Plant = rdr["Plant"].ToString();
                            vendor.Id = Convert.ToInt32(rdr["Id"].ToString());
                        }
                    }

                    return vendor;
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);

                    return null;
                }
            }
        }

        public int DeleteVendorById(int Id)
        {
            using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DeleteVendorById";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                cmd.Connection = conn;
                conn.Open();
                SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDat.SelectCommand = cmd;
                SqlDataReader rdr = cmd.ExecuteReader();
                return 0;
            }
        }

        public int DeleteMsoById(int Id)
        {
            using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DeleteMsoById";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                cmd.Connection = conn;
                conn.Open();
                SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDat.SelectCommand = cmd;
                SqlDataReader rdr = cmd.ExecuteReader();
                return 0;
            }
        }

        public Mso GetTemplate(string Name, string userName)
        {
            var cityinfo = "";
            var stateinfo = "";
            var startIndex = 6;
            var endIndex = 0;

            Mso returnedTemplate = new Mso();
            Shipping shipment = new Shipping();
            Vendor vendor = new Vendor();
            User user = new User();
            Users utilities = new Users(_MsoConnectionString);

            user = utilities.GetUserInfo(userName);
            int cashAmount = 0;
            int cashReceipt = 0;
            int checkNo = 0;

            using SqlConnection conn = new SqlConnection(_MsoConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "GetMsoTemplate";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = Name;
            cmd.Connection = conn;
            conn.Open();

            try
            {
                SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDat.SelectCommand = cmd;
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    if (!rdr["ShipTo1"].ToString().Contains("Attn") && !rdr["ShipTo2"].ToString().Contains("Attn"))
                    {
                        if (!string.IsNullOrEmpty(rdr["ShipTo1"].ToString()))
                        {
                            cityinfo = rdr["ShipTo3"].ToString();
                            city = cityinfo.Split(",");
                            stateinfo = city[1].ToString();
                            state = stateinfo.Split(" ");
                            shipment.Name = (rdr["Name"].ToString().Contains(",")) ? rdr["Name"].ToString().Replace(",", " ") : rdr["Name"].ToString();
                            shipment.Address = (rdr["ShipTo2"].ToString().Contains(",")) ? rdr["ShipTo2"].ToString().Replace(",", " ") : rdr["ShipTo2"].ToString();
                            shipment.City = city[0].ToString();
                            shipment.State = state[1].ToString();
                            shipment.Zip = (state.Count() == 3) ? state[2].ToString() : "";
                        }
                        else
                        {
                            shipment.Name = "";
                            shipment.Address = "";
                            shipment.City = "";
                            shipment.State = "";
                            shipment.Zip = "";
                        }

                        if (!string.IsNullOrEmpty(rdr["SoldTo1"].ToString()))
                        {
                            cityinfo = rdr["SoldTo3"].ToString();
                            if (cityinfo.Contains(","))
                            {
                                city = cityinfo.Split(",");
                                stateinfo = city[1].ToString();
                                state = stateinfo.Split(" ");
                            }
                            else
                            {
                                city = cityinfo.Split(" ");
                                stateinfo = cityinfo.ToString();
                                state = stateinfo.Split(" ");
                            }
                            vendor.Name = (rdr["SoldTo1"].ToString().Contains(",")) ? rdr["SoldTo1"].ToString().Replace(",", " ") : rdr["SoldTo1"].ToString();
                            vendor.Address = (rdr["SoldTo2"].ToString().Contains(",")) ? rdr["SoldTo2"].ToString().Replace(",", " ") : rdr["SoldTo2"].ToString();
                            vendor.City = city[0].ToString();
                            vendor.State = state[1].ToString();
                            vendor.Zip = (state.Count() == 3) ? state[2].ToString() : "";
                        }
                        else
                        {
                            vendor.Name = "";
                            vendor.Address = "";
                            vendor.City = "";
                            vendor.State = "";
                            vendor.Zip = "";
                        }
                    }
                    else
                    {
                        if (rdr["ShipTo1"].ToString().Contains("Attn"))
                        {
                            cityinfo = rdr["ShipTo4"].ToString();
                            city = cityinfo.Split(",");
                            stateinfo = city[1].ToString();
                            state = stateinfo.Split(" ");
                            shipment.Name = (rdr["Name"].ToString().Contains(",")) ? rdr["Name"].ToString().Replace(",", " ") : rdr["Name"].ToString();
                            shipment.City = city[0].ToString();
                            shipment.State = state[1].ToString();
                            shipment.Zip = (state.Count() == 3) ? state[2].ToString() : "";

                            if (!string.IsNullOrEmpty(rdr["SoldTo1"].ToString()))
                            {
                                cityinfo = rdr["SoldTo3"].ToString();
                                if (cityinfo.Contains(","))
                                {
                                    city = cityinfo.Split(",");
                                    stateinfo = city[1].ToString();
                                    state = stateinfo.Split(" ");
                                }
                                else
                                {
                                    city = cityinfo.Split(" ");
                                    stateinfo = cityinfo.ToString();
                                    state = stateinfo.Split(" ");
                                }
                                vendor.Name = (rdr["SoldTo1"].ToString().Contains(",")) ? rdr["SoldTo1"].ToString().Replace(",", " ") : rdr["SoldTo1"].ToString();
                                vendor.Address = (rdr["SoldTo2"].ToString().Contains(",")) ? rdr["SoldTo2"].ToString().Replace(",", " ") : rdr["SoldTo2"].ToString();
                                vendor.City = city[0].ToString();
                                vendor.State = state[1].ToString();
                                vendor.Zip = (state.Count() == 3) ? state[2].ToString() : "";
                            }
                            else
                            {
                                vendor.Name = "";
                                vendor.Address = "";
                                vendor.City = "";
                                vendor.State = "";
                                vendor.Zip = "";
                            }
                        }
                        if (rdr["ShipTo2"].ToString().Contains("Attn"))
                        {
                            cityinfo = rdr["ShipTo4"].ToString();
                            city = cityinfo.Split(",");
                            stateinfo = city[1].ToString();
                            state = stateinfo.Split(" ");
                            shipment.Name = (rdr["Name"].ToString().Contains(",")) ? rdr["Name"].ToString().Replace(",", " ") : rdr["Name"].ToString();
                            shipment.Address = (rdr["ShipTo3"].ToString().Contains(",")) ? rdr["ShipTo3"].ToString().Replace(",", " ") : rdr["ShipTo3"].ToString();
                            shipment.City = city[0].ToString();
                            shipment.State = state[1].ToString();
                            shipment.Zip = (state.Count() == 3) ? state[2].ToString() : "";
                            shipment.Name = "";
                            shipment.Address = "";
                            shipment.City = "";
                            shipment.State = "";
                            shipment.Zip = "";

                            if (!string.IsNullOrEmpty(rdr["SoldTo1"].ToString()))
                            {
                                cityinfo = rdr["SoldTo3"].ToString();
                                if (cityinfo.Contains(","))
                                {
                                    city = cityinfo.Split(",");
                                    stateinfo = city[1].ToString();
                                    state = stateinfo.Split(" ");
                                }
                                else
                                {
                                    city = cityinfo.Split(" ");
                                    stateinfo = cityinfo.ToString();
                                    state = stateinfo.Split(" ");
                                }
                                vendor.Name = (rdr["SoldTo1"].ToString().Contains(",")) ? rdr["SoldTo1"].ToString().Replace(",", " ") : rdr["SoldTo1"].ToString();
                                vendor.Address = (rdr["SoldTo2"].ToString().Contains(",")) ? rdr["SoldTo2"].ToString().Replace(",", " ") : rdr["SoldTo2"].ToString();
                                vendor.City = city[0].ToString();
                                vendor.State = state[1].ToString();
                                vendor.Zip = (state.Count() == 3) ? state[2].ToString() : "";
                            }
                            else
                            {
                                vendor.Name = "";
                                vendor.Address = "";
                                vendor.City = "";
                                vendor.State = "";
                                vendor.Zip = "";
                            }
                        }
                    }
                    cashAmount = (!string.IsNullOrEmpty(rdr["cashAmount"].ToString())) ? Convert.ToInt32(rdr["cashAmount"].ToString()) : 0;
                    cashReceipt = (!string.IsNullOrEmpty(rdr["cashReceipt"].ToString())) ? Convert.ToInt32(rdr["cashReceipt"].ToString()) : 0;
                    checkNo = (!string.IsNullOrEmpty(rdr["cashCheckNo"].ToString())) ? Convert.ToInt32(rdr["cashCheckNo"].ToString()) : 0;

                    Mso mso = new Mso
                    {
                        RequestNumber = GetNextRequestNumber(),
                        Vendor = vendor,
                        Shipment = shipment,
                        ShippingInstructions = rdr["userInstructions"].ToString(),
                        Remarks = rdr["userRemarks"].ToString(),
                        CompletedBy = rdr["appCompBy"].ToString(),
                        OtherReferenceNumber = rdr["otherRefNum"].ToString(),
                        Buyer = rdr["buyerSelect"].ToString(),
                        ReturnRep = rdr["returnRep"].ToString(),
                        CenturyRepresentative = rdr["cenRep"].ToString(),
                        RequestedByDepartment = rdr["appReqDept"].ToString(),
                        GoodsShipped = (rdr["gateGoods"].ToString() == "on") ? true : false,
                        GoodsLoaned = (rdr["gateGoodsEquip"].ToString() == "on") ? true : false,
                        GateVendor = (rdr["gateVendor"].ToString() == "on") ? true : false,
                        Other = (rdr["gateOther"].ToString() == "on") ? true : false,
                        Fob = (!string.IsNullOrEmpty(rdr["fob"].ToString())) ? Convert.ToInt32(rdr["fob"].ToString()) : 0,
                        ShipDate = (!string.IsNullOrEmpty(rdr["msoShipDate"].ToString())) ? Convert.ToDateTime(rdr["msoShipDate"].ToString()) : Convert.ToDateTime("1/1/00 00:00:00"),
                        Return = (!string.IsNullOrWhiteSpace(rdr["gateReturn"].ToString()) && rdr["gateReturn"].ToString() != " ") ? true : false,
                        Repaired = (!string.IsNullOrWhiteSpace(rdr["gateEquip"].ToString()) && rdr["gateEquip"].ToString() != " ") ? true : false,
                        CashAmount = cashAmount,
                        CashReceipt = cashReceipt,
                        CashCheckNo = checkNo,
                        CorpVenNo = rdr["corpVenNo"].ToString(),
                        TransactionType = rdr["tran_type"].ToString(),
                        ShipVia = rdr["shipviaTxt"].ToString(),
                        RequestedByName = rdr["appReqName"].ToString(),
                        BillOfLading = (!String.IsNullOrEmpty(rdr["billOfLading"].ToString())) ? rdr["billOfLading"].ToString() : null,
                        RoleId = user.RoleId,
                        SoldTo1 = rdr["SoldTo1"].ToString(),
                        SoldTo2 = rdr["SoldTo2"].ToString(),
                        SoldTo3 = rdr["SoldTo3"].ToString(),
                        SoldTo4 = rdr["SoldTo4"].ToString()
                    };

                    switch (rdr["gateGoodsEquip"].ToString())
                    {
                        case "on":
                            mso.GoodsLoaned = true;
                            break;
                    }

                    switch (rdr["gateVendor"].ToString())
                    {
                        case "on":
                            mso.VendorRemove = true;
                            break;
                    }

                    switch (rdr["gateOther"].ToString())
                    {
                        case "on":
                            mso.Other = true;
                            break;
                    }

                    if (!string.IsNullOrEmpty(rdr["fob"].ToString()))
                    {
                        switch (Convert.ToInt32(rdr["fob"].ToString()))
                        {
                            case 1:
                                mso.Fob = 1;
                                break;

                            case 2:
                                mso.Destination = rdr["fob"].ToString();
                                break;

                            case 3:
                                mso.OtherSpecify = rdr["fob"].ToString();
                                break;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(rdr["gateReturn"].ToString()) && rdr["gateReturn"].ToString() != " ")
                    {
                        mso.Return = true;
                    }
                    returnedTemplate = mso;
                }

                return returnedTemplate;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

                return null;
            }
        }

        public IList<Template> GetTemplates()
        {
            IList<Template> templates = new List<Template>();
            var cityinfo = "";
            var stateinfo = "";
            var startIndex = 6;
            var endIndex = 0;
            var venName = "";

            using SqlConnection conn = new SqlConnection(_MsoConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "GetMsoTemplates";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            conn.Open();

            try
            {
                SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDat.SelectCommand = cmd;
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    //Vendor vendor = new Vendor();
                    //var tempShipTo = rdr["Shipto1"].ToString();

                    //if (!string.IsNullOrEmpty(rdr["SoldTo1"].ToString()))
                    //{
                    //    cityinfo = rdr["SoldTo3"].ToString();
                    //    if (cityinfo.Contains(","))
                    //    {
                    //        city = cityinfo.Split(",");
                    //        stateinfo = city[1].ToString();
                    //        state = stateinfo.Split(" ");
                    //    }
                    //    else
                    //    {
                    //        city = cityinfo.Split(" ");
                    //        stateinfo = cityinfo.ToString();
                    //        state = stateinfo.Split(" ");
                    //    }

                    //    vendor = new Vendor
                    //    {
                    //        Name = !string.IsNullOrEmpty(rdr["SoldTo1"].ToString()) ? rdr["SoldTo1"].ToString() : null,
                    //        Address = !string.IsNullOrEmpty(rdr["SoldTo2"].ToString()) && rdr["SoldTo2"].ToString().Contains(",") ? rdr["SoldTo2"].ToString().Replace(",", " ") : null,
                    //        City = !string.IsNullOrEmpty(city[0].ToString()) ? city[0].ToString() : null,
                    //        State = !string.IsNullOrEmpty(state[1].ToString()) ? state[1].ToString() : null,
                    //        Zip = (state.Count() == 3) ? state[2].ToString() : null
                    //    };
                    //}

                    //vendor = GetVendorByName(tempShipTo);

                    Template template = new Template
                    {
                        RequestedByDepartment = rdr["appReqDept"].ToString(),
                        RequestedByName = rdr["appReqName"].ToString(),
                        CompletedBy = rdr["appCompBy"].ToString(),
                        Remarks = rdr["userRemarks"].ToString(),
                        ShippingInstructions = rdr["userInstructions"].ToString(),
                        CenturyRepresentative = rdr["cenRep"].ToString(),
                        VendorsRepresentative = rdr["returnRep"].ToString(),
                        SoldTo1 = rdr["Soldto1"].ToString(),
                        SoldTo2 = rdr["Soldto2"].ToString(),
                        SoldTo3 = rdr["Soldto3"].ToString(),
                        SoldTo4 = rdr["Soldto4"].ToString(),
                        Name = rdr["Name"].ToString(),
                        TransactionType = rdr["tran_type"].ToString(),
                        ShipVia = rdr["shipviaTxt"].ToString(),
                        Items = GetMsoTemplateItems((int)rdr["Id"]),
                        //Vendor = vendor,
                        Id = Convert.ToInt32(rdr["Id"].ToString())
                    };
                    Shipping shipment = new Shipping();

                    if (!string.IsNullOrEmpty(rdr["purchOrder"].ToString()))
                    {
                        template.PurchaseNumber = rdr["purchReqNum"].ToString();
                    }
                    if (!string.IsNullOrEmpty(rdr["purchOrder"].ToString()))
                    {
                        template.OrderNumber = rdr["purchOrder"].ToString();
                    }
                    if (!string.IsNullOrEmpty(rdr["otherRefNum"].ToString()))
                    {
                        template.OtherReferenceNumber = rdr["otherRefNum"].ToString();
                    }
                    if (!string.IsNullOrEmpty(rdr["woNum"].ToString()))
                    {
                        template.WorkOrderNumber = rdr["woNum"].ToString();
                    }

                    //Ship To
                    shipment.Name = rdr["Name"].ToString();
                    shipment.Address = rdr["ShipTo2"].ToString();

                    if (!String.IsNullOrEmpty(rdr["ShipTo3"].ToString()) && rdr["ShipTo3"].ToString().Contains(","))
                    {
                        cityinfo = rdr["ShipTo3"].ToString();

                        String[] city = cityinfo.Split(",");

                        stateinfo = city[1].ToString();
                        String[] state = stateinfo.Split(" ");

                        shipment.City = city[0].ToString();
                        shipment.State = state[1].ToString();
                        var zipcode = (state.Count() == 3) ? state[2].ToString() : null;
                        shipment.Zip = zipcode;
                    }
                    else
                    {
                        cityinfo = rdr["ShipTo3"].ToString();
                        if (!string.IsNullOrEmpty(cityinfo))
                        {
                            String[] city = cityinfo.Split(" ");

                            shipment.City = city[0].ToString();
                            shipment.State = city[1].ToString();
                            var zipcode = (city.Count()! > 3) ? city[2].ToString() : null;
                            shipment.Zip = zipcode;
                        }
                    }

                    template.Shipment = shipment;

                    switch (rdr["gateGoodsEquip"].ToString())
                    {
                        case "on":
                            template.GoodsLoaned = true;
                            break;
                    }

                    switch (rdr["gateVendor"].ToString())
                    {
                        case "on":
                            template.VendorRemove = true;
                            break;
                    }

                    switch (rdr["gateOther"].ToString())
                    {
                        case "on":
                            template.Other = true;
                            break;
                    }

                    if (!string.IsNullOrEmpty(rdr["fob"].ToString()))
                    {
                        switch (Convert.ToInt32(rdr["fob"].ToString()))
                        {
                            case 1:
                                GetVendorByRequest(template.RequestNumber);
                                break;

                            case 2:
                                template.Destination = rdr["fob"].ToString();
                                break;

                            case 3:
                                template.OtherSpecify = rdr["fob"].ToString();
                                break;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(rdr["gateReturn"].ToString()) && rdr["gateReturn"].ToString() != " ")
                    {
                        template.Return = true;
                    }

                    templates.Add(template);
                }

                IEnumerable<Template> sortedEnum = templates.Reverse().OrderBy(f => f.Shipment.Name);
                IList<Template> sortedTemplate = sortedEnum.ToList();

                return sortedTemplate;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

                return null;
            }
        }

        public int GetVendorId(string Name)
        {
            using SqlConnection conn = new SqlConnection(_MsoConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "GetVendorByID";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = Name.Trim();
            cmd.Connection = conn;
            conn.Open();
            try
            {
                SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDat.SelectCommand = cmd;
                SqlDataReader rdr = cmd.ExecuteReader();

                Vendor vendor = new Vendor();

                while (rdr.Read())
                {
                    if (rdr["Name"].ToString() != "")
                    {
                        vendor.Id = Convert.ToInt32(rdr["Id"].ToString());
                    }
                }

                return vendor.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

                return 0;
            }
        }

        public Vendor GetVendorByName(string Name)
        {
            using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "GetVendorByName";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = Name.Trim();
                cmd.Connection = conn;
                conn.Open();
                try
                {
                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                    sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDat.SelectCommand = cmd;
                    SqlDataReader rdr = cmd.ExecuteReader();

                    Vendor vendor = new Vendor();

                    while (rdr.Read())
                    {
                        if (rdr["Name"].ToString() != "")
                        {
                            vendor.Name = rdr["Name"].ToString().Replace(".", "").ToString().Replace(",", "");
                            vendor.Address = rdr["Address"].ToString();
                            vendor.City = rdr["City"].ToString();
                            vendor.State = rdr["State"].ToString();
                            vendor.Zip = rdr["Zip"].ToString();
                            vendor.Plant = rdr["Plant"].ToString();
                            vendor.Id = Convert.ToInt32(rdr["Id"].ToString());
                        }
                    }
                    if (vendor == null)
                    {
                        vendor.Name = "Empty";
                    }

                    return vendor;
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);

                    return null;
                }
            }
        }

        public Vendor GetVendorById(int Id)
        {
            using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "GetVendorById";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                cmd.Connection = conn;
                conn.Open();
                SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDat.SelectCommand = cmd;
                SqlDataReader rdr = cmd.ExecuteReader();

                Vendor vendor = new Vendor();

                try
                {
                    while (rdr.Read())
                    {
                        if (rdr["Name"].ToString() != "")
                        {
                            vendor.Name = rdr["Name"].ToString();//.Replace(".", "").ToString();
                            vendor.Address = rdr["Address"].ToString();
                            vendor.City = rdr["City"].ToString();
                            vendor.State = rdr["State"].ToString();
                            vendor.Zip = rdr["Zip"].ToString();
                            vendor.Plant = rdr["Plant"].ToString();
                            vendor.Id = Convert.ToInt32(rdr["Id"].ToString());
                        }
                    }
                    if (string.IsNullOrEmpty(vendor.Name))
                    {
                        vendor.Name = "Empty";
                    }
                    return vendor;
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);

                    return null;
                }
            }
        }

        public IList<Item> GetMsoItems(int requestNumber)
        {
            IList<Item> items = new List<Item>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetMsoItems", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("reqNum", SqlDbType.Int).Value = requestNumber;
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Item item = new Item();

                            item.ItemCount = Convert.ToInt32(rdr["matItem"].ToString());
                            item.Description = rdr["matDesc"].ToString();
                            item.Quantity = Convert.ToInt32(rdr["matQty"]);
                            item.Unit = rdr["matUnit"].ToString();
                            if (string.IsNullOrEmpty(rdr["matGross"].ToString()))
                            {
                                item.Gross = 0;
                            }
                            else
                            {
                                item.Gross = Convert.ToDouble(rdr["matGross"]);
                            }
                            if (string.IsNullOrEmpty(rdr["matNet"].ToString()))
                            {
                                item.Net = 0;
                            }
                            else
                            {
                                item.Net = Convert.ToDouble(rdr["matNet"]);
                            }
                            if (string.IsNullOrEmpty(rdr["matTare"].ToString()))
                            {
                                item.Tare = 0;
                            }
                            else
                            {
                                item.Tare = Convert.ToDouble(rdr["matTare"].ToString());
                            }
                            item.Value = rdr["matValue"].ToString();
                            item.RequestNumber = Convert.ToInt32(rdr["reqNum"].ToString());
                            item.StockNumber = rdr["matStockItem"].ToString();
                            items.Add(item);
                        }
                    }
                    IEnumerable<Item> sortedEnum = items.OrderBy(f => f.ItemCount);
                    IList<Item> sortedItems = sortedEnum.ToList();
                    return sortedItems;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public IList<Item> GetMsoTemplateItems(int requestNumber)
        {
            IList<Item> items = new List<Item>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetMsoTemplateItems", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("reqNum", SqlDbType.Int).Value = requestNumber;
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Item item = new Item();

                            item.ItemCount = Convert.ToInt32(rdr["matItem"].ToString());
                            item.Description = rdr["matDesc"].ToString();
                            item.Quantity = Convert.ToInt32(rdr["matQty"]);
                            item.Unit = rdr["matUnit"].ToString();
                            if (string.IsNullOrEmpty(rdr["matGross"].ToString()))
                            {
                                item.Gross = 0;
                            }
                            else
                            {
                                item.Gross = Convert.ToDouble(rdr["matGross"]);
                            }
                            if (string.IsNullOrEmpty(rdr["matNet"].ToString()))
                            {
                                item.Net = 0;
                            }
                            else
                            {
                                item.Net = Convert.ToDouble(rdr["matNet"]);
                            }
                            if (string.IsNullOrEmpty(rdr["matTare"].ToString()))
                            {
                                item.Tare = 0;
                            }
                            else
                            {
                                item.Tare = Convert.ToDouble(rdr["matTare"].ToString());
                            }
                            item.Value = rdr["matValue"].ToString();

                            item.RequestNumber = Convert.ToInt32(rdr["id"].ToString());
                            item.StockNumber = rdr["matStockItem"].ToString();
                            items.Add(item);
                        }
                    }
                    IEnumerable<Item> sortedEnum = items.OrderBy(f => f.ItemCount);
                    IList<Item> sortedItems = sortedEnum.ToList();
                    return sortedItems;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public string GetLocationFromID(string ID)
        {
            switch (ID)
            {
                case "1":
                    return "Hawesville";

                case "2":
                    return "Sebree";

                case "3":
                    return "Mt. Holly";

                case "4":
                    return "Iceland";

                case "5":
                    return "Nashville";

                default:
                    return "Unknown";
            }
        }

        public string UpdateVendor(Vendor vendor)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    DataTable dtblVendor = new DataTable() { TableName = "Edit" };
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "UpdateVendor";
                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("Id", vendor.Id);
                    cmd.Parameters.Add("@Plant", SqlDbType.VarChar).Value = vendor.Plant;
                    cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = vendor.Name;

                    if (vendor.Address != null)
                    {
                        cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendor.Address;
                    }
                    else
                    {
                        cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = null;
                    }
                    if (vendor.City != null)
                    {
                        cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = vendor.City;
                    }
                    else
                    {
                        cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = null;
                    }
                    if (vendor.State != null)
                    {
                        cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = vendor.State;
                    }
                    else
                    {
                        cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = null;
                    }
                    if (vendor.Zip != null)
                    {
                        cmd.Parameters.Add("@Zip", SqlDbType.Int).Value = vendor.Zip;
                    }
                    else
                    {
                        cmd.Parameters.Add("@Zip", SqlDbType.Int).Value = null;
                    }

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    return "Vendor";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public string DeleteVendor(int id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("DeleteVendor", conn);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;

                    cmd.ExecuteNonQuery();
                }

                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public string CreateVendor(Vendor vendor)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "CreateVendor";
                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                    conn.Open();
                    cmd.Connection = conn;

                    cmd.Parameters.Add("@Plant", SqlDbType.VarChar).Value = vendor.Plant;
                    cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = vendor.Name;
                    cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendor.Address;
                    cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = vendor.City;
                    cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = vendor.State;
                    cmd.Parameters.Add("@Zip", SqlDbType.BigInt).Value = vendor.Zip;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    return "Vendor";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public IList<Vendor> GetVendors(string name)
        {
            Users utilities = new Users(_MsoConnectionString);
            User user = new User();

            user = utilities.GetUserInfo(name);

            using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
            {
                List<Vendor> vendors = new List<Vendor>();
                DataTable dtblVendors = new DataTable() { TableName = "Edit" };
                SqlCommand cmd = new SqlCommand();

                var Plant = GetPlant(name);
                Log.Information(name);
                cmd.CommandText = "GetVendors";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Plant", SqlDbType.VarChar).Value = Plant;
                cmd.Connection = conn;
                conn.Open();
                SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDat.SelectCommand = cmd;
                SqlDataReader rdr = cmd.ExecuteReader();

                try
                {
                    while (rdr.Read())
                    {
                        Vendor vendor = new Vendor();
                        //user = utilities.GetUserInfo(name);
                        if (rdr["Name"].ToString() != "")
                        {
                            vendor.Name = rdr["Name"].ToString();//.Replace(".", "").ToString();
                            vendor.Address = rdr["Address"].ToString().Replace(".", "").ToString();
                            vendor.City = rdr["City"].ToString();
                            vendor.State = rdr["State"].ToString();
                            vendor.Zip = rdr["Zip"].ToString();
                            vendor.Plant = rdr["plant"].ToString();
                            vendor.Id = Convert.ToInt32(rdr["Id"].ToString());
                            vendor.RoleId = user.RoleId;
                        }

                        vendors.Add(vendor);
                    }
;
                    return (IList<Vendor>)vendors;
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);

                    return null;
                }
            }
        }

        public DataTable GetAhoc(string adhoc, string msoNumber, string StartDate, string EndDate)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                    DataTable dtblMso = new DataTable();
                    conn.Open();

                    cmd.Connection = conn;
                    if (adhoc.Contains("Waste"))
                    {
                        cmd.CommandText = "wastereport";
                    }

                    cmd.CommandType = CommandType.StoredProcedure;

                    if (!String.IsNullOrEmpty(adhoc) && !adhoc.Contains("Waste"))
                    {
                        cmd.Parameters.AddWithValue("adhoc", adhoc);
                        cmd.Parameters.AddWithValue("msoNumber", msoNumber);
                    }

                    cmd.Parameters.AddWithValue("startDate", StartDate);
                    cmd.Parameters.AddWithValue("endDate", EndDate);

                    sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDat.SelectCommand = cmd;

                    sqlDat.Fill(dtblMso);
                    return dtblMso;
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex.ToString());
                return null;
            }
        }

        public Waste GetWaste(string RequestNumber)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);

                    Waste waste = new Waste();
                    conn.Open();

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("RequestNumber", RequestNumber);
                    cmd.CommandText = "GetWaste";

                    SqlDataReader rdr = cmd.ExecuteReader();

                    try
                    {
                        while (rdr.Read())
                        {
                            waste.RequestNumber = Convert.ToInt32(rdr["msonum"].ToString());
                            waste.Description = rdr["wasteDesc"].ToString();
                            waste.CreationDate = rdr["msoDate"].ToString();
                            waste.Weight = Convert.ToInt32(rdr["wasteWeight"].ToString());
                            waste.Hauler = rdr["hauler"].ToString();
                        }
;
                        return waste;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.Message);

                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex.ToString());
                return null;
            }
        }

        public IList<Waste> GetWastes()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                    DataTable dtblMso = new DataTable();
                    IList<Waste> wastes = new List<Waste>();
                    conn.Open();

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "GetWastes";

                    sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDat.SelectCommand = cmd;
                    SqlDataReader rdr = cmd.ExecuteReader();

                    try
                    {
                        while (rdr.Read())
                        {
                            Waste waste = new Waste();

                            waste.RequestNumber = Convert.ToInt32(rdr["msonum"].ToString());
                            waste.Description = rdr["wasteDesc"].ToString();
                            waste.CreationDate = rdr["msoDate"].ToString();
                            waste.Weight = Convert.ToInt32(rdr["wasteWeight"].ToString());
                            waste.Hauler = rdr["hauler"].ToString();

                            wastes.Add(waste);
                        }
;
                        IEnumerable<Waste> sortedEnum = wastes.OrderBy(f => f.RequestNumber);
                        IList<Waste> sortedWaste = sortedEnum.ToList();
                        return sortedWaste;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.Message);

                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex.ToString());
                return null;
            }
        }

        public IList<Waste> WasteReport(string StartDate, string EndDate)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                    DataTable dtblMso = new DataTable();
                    IList<Waste> wastes = new List<Waste>();
                    conn.Open();

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "WasteReport";
                    cmd.Parameters.AddWithValue("startDate", StartDate);
                    cmd.Parameters.AddWithValue("endDate", EndDate);

                    sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDat.SelectCommand = cmd;
                    SqlDataReader rdr = cmd.ExecuteReader();

                    try
                    {
                        while (rdr.Read())
                        {
                            Waste waste = new Waste();

                            waste.RequestNumber = Convert.ToInt32(rdr["msonum"].ToString());
                            waste.Description = rdr["wasteDesc"].ToString();
                            waste.CreationDate = rdr["msoDate"].ToString();
                            waste.Weight = Convert.ToInt32(rdr["wasteWeight"].ToString());
                            waste.Hauler = rdr["hauler"].ToString();

                            wastes.Add(waste);
                        }
;
                        IEnumerable<Waste> sortedEnum = wastes.OrderBy(f => f.RequestNumber);
                        IList<Waste> sortedWaste = sortedEnum.ToList();
                        return sortedWaste;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.Message);

                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex.ToString());
                return null;
            }
        }

        public DataTable GetAdhoc(string adhoc, string msoNumber, string StartDate, string EndDate)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter sqlDat = new SqlDataAdapter(cmd);
                    DataTable dtblMso = new DataTable();
                    conn.Open();

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "AdhocMSO";

                    cmd.Parameters.AddWithValue("adhoc", adhoc);
                    cmd.Parameters.AddWithValue("msoNumber", msoNumber);

                    cmd.Parameters.AddWithValue("startDate", StartDate);
                    cmd.Parameters.AddWithValue("endDate", EndDate);

                    sqlDat.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDat.SelectCommand = cmd;

                    sqlDat.Fill(dtblMso);
                    return dtblMso;
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex.ToString());
                return null;
            }
        }
    }
}