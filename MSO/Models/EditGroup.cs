﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MSO.DataAccess;

namespace MSO.Models
{
    public class EditGroup
    {
        private readonly IConfiguration _config;
        private string _MsoConnectionString;

        public EditGroup(IConfiguration config)
        {
            _config = config;
        }

        public int Id(string userName)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            return sQLHelper.GetUserAccess(userName);
        }
    }
}