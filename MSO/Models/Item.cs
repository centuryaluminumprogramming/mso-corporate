﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MSO.Models
{
    public class Item
    {
        [Required]
        public int RequestNumber { get; set; }
        public string StockNumber { get; set; }

        [Required]
        public string Description { get; set; }
        public double Quantity { get; set; }
        public string Unit { get; set; }
        public double Gross { get; set; }
        public double Tare { get; set; }
        public double Net { get; set; }
        public string Value { get; set; }
        public int ItemCount { get; set; }

    }
}
