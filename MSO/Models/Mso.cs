﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MSO.Models
{
    public class Mso
    {
        [Required(ErrorMessage = "A Request Number is required")]
        public int RequestNumber { get; set; }

        public string OrderNumber { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime ShipDate { get; set; }

        public string RequestedByName { get; set; }

        public string TransactionType { get; set; }

        public string BillOfLading { get; set; }

        public string OtherSpecify { get; set; }

        public bool PrepayChargeInvoice { get; set; }

        public bool PrepayAllow { get; set; }

        public bool Collect { get; set; }

        public bool Return { get; set; }

        public bool Pass { get; set; }

        public bool Exchange { get; set; }
        public bool Print { get; set; }

        public bool Repaired { get; set; }

        public bool GoodsShipped { get; set; }

        public bool GoodsLoaned { get; set; }

        public bool GateVendor { get; set; }

        public bool VendorRemove { get; set; }

        public string Remarks { get; set; }

        public string ShippingInstructions { get; set; }

        public string CompletedBy { get; set; }

        public string Buyer { get; set; }

        public string VendorsRepresentative { get; set; }

        public string CenturyRepresentative { get; set; }

        public int CashAmount { get; set; }

        public int CashCheckNo { get; set; }

        public int CashReceipt { get; set; }

        public int CashReceived { get; set; }

        public bool Credit { get; set; }

        public bool Other { get; set; }

        public string PurchaseNumber { get; set; }

        public string WorkOrderNumber { get; set; }

        public string OtherReferenceNumber { get; set; }

        public string RequestedByDepartment { get; set; }

        public string SoldTo1 { get; set; }

        public string SoldTo2 { get; set; }

        public string SoldTo3 { get; set; }

        public string SoldTo4 { get; set; }

        public string StockItem { get; set; }

        public string Destination { get; set; }

        public int TareWeight { get; set; }

        public int Fob { get; set; }

        public int Freight { get; set; }

        public Vendor Vendor { get; set; }

        public IList<Item> Items { get; set; }

        public string ShipVia { get; set; }

        public Shipping Shipment { get; set; }

        public string CorpVenNo { get; set; }

        public string ReturnRep { get; set; }

        public bool GateEquip { get; set; }

        public int RoleId { get; set; }

        public IList<Vendor> Vendors { get; set; }

        public IList<Plant> Plants { get; set; }

        public Item item { get; set; }

        public string Status { get; set; }

        public string Location { get; set; }
    }
}