﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSO.Models
{
    public class MsoViewModel
    {
        public Mso Mso { get; set; }
        public IList<Vendor> Vendors { get; set; }
    }
}