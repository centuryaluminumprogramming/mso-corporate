﻿using System.Collections.Generic;


namespace MSO.Models
{
    public class NewMsoViewModel
    {
        public Mso Mso { get; set; }
        public IList<Item> Items { get; set; }
    }
}
