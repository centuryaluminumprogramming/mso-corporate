﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSO.Models
{
    public class Plant
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}