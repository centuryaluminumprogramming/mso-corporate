﻿using System.Collections.Generic;

namespace MSO.Models
{
    public class Search
    {
        public SearchOptions SearchOption { get; set; }
        public IList<Mso> MSOs { get; set; }
    }
}