﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace MSO.Models
{
    public class SearchOptions
    {
        //[Display(Name = "Field to Search")]
        public string SearchType { get; set; }

        public string SearchString { get; set; }

        public string startDate { get; set; }
        public string endDate { get; set; }
        public IEnumerable<SelectListItem> SearchItems { get; set; }
    }
}