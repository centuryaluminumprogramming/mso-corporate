﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MSO.Models
{
    public class Shipping
    {
        public string Name { get; set; }

        [Required(ErrorMessage = "An address is required.")]
        public string Address { get; set; }

        [Required(ErrorMessage = "A city is required.")]
        public string City { get; set; }

        [Required(ErrorMessage = "A state is required.")]
        public string State { get; set; }

        [Required(ErrorMessage = "A zip code is required.")]
        public string Zip { get; set; }

        public string ShipVia { get; set; }
        public string Attention { get; set; }
    }
}