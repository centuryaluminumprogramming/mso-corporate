﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MSO.Tools;
using Microsoft.Extensions.Configuration;

namespace MSO.Models
{
    public class User
    {
        public string UserName { get; set; }
        public string GUID { get; set; }
        public string Plant { get; set; }
        public int RoleId { get; set; }
    }
}