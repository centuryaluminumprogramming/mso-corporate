﻿using System.ComponentModel.DataAnnotations;

namespace MSO.Models
{
    public class Vendor
    {
        [Required(ErrorMessage = "A name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "An address is required.")]
        public string Address { get; set; }

        [Required(ErrorMessage = "A city is required.")]
        public string City { get; set; }

        [Required(ErrorMessage = "A state is required.")]
        public string State { get; set; }

        [Required(ErrorMessage = "A zip code is required.")]
        public string Zip { get; set; }

        [Required(ErrorMessage = "An plant is required.")]
        public string Plant { get; set; }

        public string Attention { get; set; }
        public int Id { get; set; }
        public int RoleId { get; set; }
    }
}