﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSO.Models
{
    public class Waste
    {
        public int RequestNumber { get; set; }
        public string CreationDate { get; set; }
        public string Hauler { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
    }
}