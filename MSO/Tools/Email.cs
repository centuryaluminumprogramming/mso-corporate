﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Serilog;

namespace MSO.Tools
{
    public class Email
    {
        public System.IO.Stream SendMail(Stream ReportData, string FileName, string ToAddress)
        {
            try
            {
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("webmail.centuryaluminum.com")
                {
                    EnableSsl = false,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential("processControl", "sendMyMail1")
                };

                System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage();
                System.IO.Stream ms = ReportData;
                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(ms, new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Plain));
                attachment.ContentDisposition.FileName = FileName;

                mm.To.Add(new MailAddress(ToAddress));
                mm.From = new System.Net.Mail.MailAddress("CustomReport@centuryaluminum.com");
                mm.Body = "Attached custom report.";
                mm.Subject = "MSO Custom Report";
                mm.Attachments.Add(attachment);

                smtp.Send(mm);

                return ms;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }
    }
}