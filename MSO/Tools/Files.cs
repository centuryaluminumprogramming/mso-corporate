﻿using Microsoft.Extensions.Configuration;
using MSO.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using System.Data;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using iTextSharp.text.pdf;

namespace MSO.Tools
{
    public class Files
    {
        private readonly IConfiguration _config;
        private static readonly StringBuilder sb = new StringBuilder();
        private static string _MsoConnectionString;

        public Files(IConfiguration config)
        {
            _config = config;
        }

        public Files(string MsoSQLConnection)
        {
            _MsoConnectionString = MsoSQLConnection;
        }

        public static System.Web.Mvc.FileResult DownloadFile(string file)
        {
            // Get the temp folder and file path in server
            string fullPath = Path.Combine(("c:/temp"), file);

            return new System.Web.Mvc.FilePathResult(Path.GetFileName(fullPath), "application/force-download");
        }

        public IList<Models.Waste> BuildWasteFile(string StartDate, string EndDate)
        {
            try
            {
                IList<Models.Waste> records = new List<Models.Waste>();
                SQLHelper sQLHelper = new SQLHelper(_MsoConnectionString);
                records = sQLHelper.WasteReport(StartDate, EndDate);

                return records;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public bool DownloadAdhocFile(string adhoc, string msoNumber, string StartDate, string EndDate, string user, string file)
        {
            bool hasData = false;
            int userLength = 0;

            userLength = Convert.ToInt32(user.Length.ToString());

            user = user.Substring(8, userLength - 8).ToString();

            try
            {
                var adhocend = adhoc.Length;
                var adhocstart = 0;
                var adhocLength = adhoc.Substring(adhocstart, (adhocend - 1));
                List<string> records = new List<string>();
                SQLHelper sQLHelper = new SQLHelper(_MsoConnectionString);
                var data = sQLHelper.GetAdhoc(adhocLength, msoNumber, StartDate, EndDate);
                int colcount = 0;
                string adhoccolumn = "";

                foreach (DataRow colrow in data.Rows)
                {
                    DataColumnCollection columns = colrow.Table.Columns;
                    if (colcount == 0)
                    {
                        foreach (DataColumn column in columns)
                        {
                            if (String.IsNullOrEmpty(adhoccolumn))
                            {
                                adhoccolumn = column.ColumnName;
                            }
                            else
                            {
                                adhoccolumn = adhoccolumn + "," + column.ColumnName;
                            }
                        }

                        sb.AppendLine(adhoccolumn);
                        colcount = 1;
                    }
                }
                foreach (DataRow row in data.Rows)
                {
                    hasData = true;
                    List<string> fields = new List<string>(row.ItemArray.Select(field => field.ToString().Replace(",", "")));
                    sb.AppendLine(string.Join(",", fields));
                }
                if (hasData)
                {
                    if (File.Exists("C:\\Users\\" + user + "\\Downloads\\" + file + ".csv"))
                    {
                        File.Delete("C:\\Users\\" + user + "\\Downloads\\" + file + ".csv");
                    }

                    File.WriteAllText("C:\\Users\\" + user + "\\Downloads\\" + file + ".csv", string.Join(",", sb.ToString()));

                    Log.Information(user + " downloaded " + file + ".csv on " + "[ " + DateTime.Now + " ]");
                }

                return hasData;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " for " + user + " attempting to download " + file + ".csv");
                return hasData;
            }
        }

        public void DownloadReport(string StartDate, string EndDate, string user)
        {
            Utilities utilities = new Utilities(_MsoConnectionString);
            IList<Models.Waste> waste = new List<Models.Waste>();
            user = System.Security.Claims.ClaimsPrincipal.Current.Identity.Name;
            waste = DownloadWasteExcel(StartDate, EndDate, user);
            return;
        }

        public IList<Models.Waste> DownloadWasteExcel(string StartDate, string EndDate, string user)
        {
            SQLHelper sQLHelper = new SQLHelper(_MsoConnectionString);
            IList<Models.Waste> wastes = new List<Models.Waste>();
            StringBuilder sb = new StringBuilder();

            int userLength = 0;
            userLength = Convert.ToInt32(user.Length.ToString());

            user = user.Substring(8, userLength - 8).ToString();
            EndDate = StartDate.Substring(11, 10).ToString();
            StartDate = StartDate.Substring(0, 10).ToString();

            try
            {
                wastes = BuildWasteFile(StartDate, EndDate);

                sb.AppendLine(string.Join(",", "MsoNumber,MsoDate,Hauler,Description,Weight"));

                foreach (Models.Waste item in wastes)
                {
                    sb.AppendLine(string.Join(",", item.RequestNumber, item.CreationDate, item.Hauler.Replace(",", " ").ToString(), item.Description, item.Weight));
                }
                if (File.Exists("C:\\Users\\" + user + "\\Downloads\\wastereport.csv"))
                {
                    File.Delete("C:\\Users\\" + user + "\\Downloads\\wastereport.csv");
                }
                File.WriteAllText("C:\\Users\\" + user + "\\Downloads\\wastereport.csv", sb.ToString());
                Log.Information(user + " downloaded wastereport.csv on " + "[ " + DateTime.Now + " ]");

                return wastes;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " for " + user + " attempting to download wastereport.csv.");
                return wastes;
            }
        }

        public string CreatePdfFile(Models.Mso mso)
        {
            try
            {
                if (!string.IsNullOrEmpty(mso.RequestNumber.ToString()))
                {
                    //Files files = new Files(_config["MsoSQLConnection"]);
                    string currentDirectory = Directory.GetCurrentDirectory();

                    string pdfTemplate = currentDirectory + "\\" + mso.Location + "\\ViewReport.pdf";
                    string returnbool = "";

                    var ourPlant = "";
                    var destination = "";
                    var otherSpecify = "";
                    var charge = "";
                    var allow = "";
                    var collect = "";
                    var cash = "";
                    var credit = "";
                    var creditMemo = "";
                    var gatePass = "";
                    var shipToExtended = "";
                    var soldToExtended = "";

                    /// Setting default path for printed file
                    string newFile = currentDirectory + "\\Printed\\" + mso.RequestNumber + ".pdf";

                    returnbool = mso.Return.ToString();

                    PdfReader pdfReader = new PdfReader(pdfTemplate);
                    PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                    AcroFields pdfFormFields = pdfStamper.AcroFields;

                    if (mso.Vendor != null)
                    {
                        shipToExtended = mso.Vendor.City + ", " + mso.Vendor.State + ", " + mso.Vendor.Zip;
                    }
                    soldToExtended = mso.SoldTo3 + ", " + mso.SoldTo4;

                    if (!string.IsNullOrEmpty(mso.Fob.ToString()))
                    {
                        switch (mso.Fob.ToString())
                        {
                            case "1":
                                ourPlant = "X";
                                break;

                            case "2":
                                destination = "X";
                                break;

                            case "3":
                                otherSpecify = "X";
                                break;
                        }
                    }

                    if (!string.IsNullOrEmpty(mso.Freight.ToString()))
                    {
                        switch (mso.Freight.ToString())
                        {
                            case "1":
                                charge = "X";
                                break;

                            case "2":
                                allow = "X";
                                break;

                            case "3":
                                collect = "X";
                                break;
                        }
                    }
                    if (!string.IsNullOrEmpty(mso.TransactionType.ToString()))
                    {
                        switch (mso.TransactionType.ToString())
                        {
                            case "1":
                                cash = Convert.ToInt32(mso.TransactionType) == 1 ? "X" : null;
                                break;

                            case "2":
                                credit = Convert.ToInt32(mso.TransactionType) == 2 ? "X" : null;
                                break;

                            case "4":
                                gatePass = Convert.ToInt32(mso.TransactionType) == 4 ? "X" : null;
                                break;

                            case "null":
                                break;
                        }
                    }

                    var vendorReturn = mso.Return == true ? "X" : "";
                    var repaired = mso.Repaired == true ? "X" : "";
                    var goods = mso.GoodsShipped == true ? "X" : "";
                    var loan = mso.GoodsLoaned == true ? "X" : "";
                    var removed = mso.VendorRemove == true ? "X" : "";
                    var other = mso.Other == true ? "X" : "";
                    var creationDate = (!string.IsNullOrEmpty(mso.CreationDate.ToString())) ? Convert.ToDateTime(mso.CreationDate).ToShortDateString() : Convert.ToDateTime(mso.ShipDate).ToShortDateString();
                    var plantCode = 0;

                    switch (mso.Location.ToLowerInvariant())
                    {
                        case "hawesville":
                            plantCode = 9150;
                            break;

                        case "sebree":
                            plantCode = 9050;
                            break;

                        case "mt. holly":
                            plantCode = 9010;
                            break;
                    }
                    pdfFormFields.SetField("PLANT CODE", plantCode.ToString());
                    pdfFormFields.SetField("MSO NO", mso.RequestNumber.ToString());
                    pdfFormFields.SetField("MSO DATE", creationDate);
                    pdfFormFields.SetField("MSO SHIP DATE", Convert.ToDateTime(mso.ShipDate).ToShortDateString());
                    pdfFormFields.SetField("Purchase Order NO", mso.PurchaseNumber);
                    pdfFormFields.SetField("Other Ref NO", mso.OtherReferenceNumber);
                    pdfFormFields.SetField("Work Order Number", mso.WorkOrderNumber);
                    pdfFormFields.SetField("REQ NO", mso.OrderNumber);
                    pdfFormFields.SetField("BILL OF LADING NO", mso.BillOfLading);
                    pdfFormFields.SetField("SHIP VIA", mso.ShipVia);
                    pdfFormFields.SetField("SHIP NAME", mso.Vendor.Name);
                    pdfFormFields.SetField("SHIP ADDRESS", mso.Vendor.Address);
                    pdfFormFields.SetField("SHIP EXTENDED", shipToExtended);
                    pdfFormFields.SetField("SOLD NAME", mso.SoldTo1);
                    pdfFormFields.SetField("SOLD ADDRESS", mso.SoldTo2);
                    pdfFormFields.SetField("SOLD EXTENDED", soldToExtended);
                    pdfFormFields.SetField("Requested by NAME", mso.RequestedByName.ToString());
                    pdfFormFields.SetField("Requested by DEPT", mso.RequestedByDepartment.ToString());
                    pdfFormFields.SetField("AMOUNT", mso.CashAmount.ToString());
                    pdfFormFields.SetField("RECEIPT NO", mso.CashReceipt.ToString());
                    pdfFormFields.SetField("DATE RECEIVED", mso.CashReceived.ToString());
                    pdfFormFields.SetField("CHECK NO", mso.CashCheckNo.ToString());
                    pdfFormFields.SetField("VENDOR NO", mso.CorpVenNo);

                    /// The form's checkboxes
                    pdfFormFields.SetField("OUR PLANT", ourPlant.ToString());
                    pdfFormFields.SetField("DESTINATION", destination.ToString());
                    pdfFormFields.SetField("OTHER SPECIFY", otherSpecify.ToString());
                    pdfFormFields.SetField("PREPAY CHARGE", charge.ToString());
                    pdfFormFields.SetField("PREPAY ALLOW", allow.ToString());
                    pdfFormFields.SetField("COLLECT", collect.ToString());
                    pdfFormFields.SetField("REPAIRED", repaired.ToString());
                    pdfFormFields.SetField("SHIPPED", goods.ToString());
                    pdfFormFields.SetField("CASH SALE", cash.ToString());
                    pdfFormFields.SetField("CREDIT SALE", credit.ToString());
                    pdfFormFields.SetField("CREDIT MEMO", creditMemo.ToString());
                    pdfFormFields.SetField("GATE PASS", gatePass.ToString());

                    /// The form's Items
                    SQLHelper sQLHelper = new SQLHelper(_MsoConnectionString);

                    int itemcount = sQLHelper.GetMsoItems(Convert.ToInt32(mso.RequestNumber)).Count();

                    foreach (var item in sQLHelper.GetMsoItems(Convert.ToInt32(mso.RequestNumber)))
                    {
                        pdfFormFields.SetField("CA STOCK Row" + itemcount, item.StockNumber);
                        pdfFormFields.SetField("DescriptionRow" + itemcount, item.Description);
                        pdfFormFields.SetField("QtyRow" + itemcount, item.Quantity.ToString());
                        pdfFormFields.SetField("UnitRow" + itemcount, item.Unit);
                        pdfFormFields.SetField("Gross WGTRow" + itemcount, item.Gross.ToString());
                        pdfFormFields.SetField("Tare WGTRow" + itemcount, item.Tare.ToString());
                        pdfFormFields.SetField("Net WGTRow" + itemcount, item.Net.ToString());
                        pdfFormFields.SetField("Mat ValueRow" + itemcount, item.ItemCount.ToString());

                        itemcount = itemcount - 1;
                    }

                    pdfFormFields.SetField("Remarks", mso.Remarks);
                    pdfFormFields.SetField("Shipping Instructions", mso.ShippingInstructions);
                    pdfFormFields.SetField("Shipment Completed By", mso.CompletedBy);
                    pdfFormFields.SetField("Buyer", mso.Buyer);
                    pdfFormFields.SetField("NAME OF VENDORS REPRESENTITIVE", mso.ReturnRep);
                    pdfFormFields.SetField("REPRESENTATIVE WHO AUTHORIZED RETURN", mso.CenturyRepresentative);

                    /// report by reading values from completed PDF
                    /// flatten the form to remove editting options, set it to false
                    /// to leave the form open to subsequent manual edits
                    pdfStamper.FormFlattening = false;
                    /// close the pdf

                    pdfStamper.Close();

                    return newFile;
                }
                else
                {
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return "";
            }
        }
    }
}