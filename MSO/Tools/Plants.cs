﻿using Microsoft.Extensions.Configuration;
using MSO.DataAccess;
using MSO.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;

namespace MSO.Tools
{
    public class Plants
    {
        private static IConfiguration _config;
        private static string _MsoConnectionString;

        public Plants(string MsoSQLConnection)
        {
            _MsoConnectionString = MsoSQLConnection;
        }

        public Plants(IConfiguration config)
        {
            _config = config;
        }

        public static Plants UpdatePlant(string plant, User user)
        {
            SQLHelper sQLHelper = new SQLHelper(_MsoConnectionString);
            sQLHelper.UpdatePlant(user.UserName, user.GUID, plant);
            return null;
        }

        public static string GetPlant(string userName)
        {
            DataSet ds = new DataSet("Plant");
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            string userGUID;

            try
            {
                if (!userName.Equals(null))
                {
                    using var context = new PrincipalContext(ContextType.Domain, "Century");
                    UserPrincipal User = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);

                    userGUID = Convert.ToString(User.Guid);
                    Log.Information("Found user " + userName + " with the GUID: " + userGUID);
                }
                else
                {
                    using var context = new PrincipalContext(ContextType.Domain, "Century");
                    UserPrincipal User = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, "");

                    userGUID = Convert.ToString(User.Guid);

                    using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                    {
                        SqlCommand sqlComm = new SqlCommand("UpdatePlant", conn);
                        sqlComm.Parameters.AddWithValue("userGUID", userGUID);
                        sqlComm.Parameters.AddWithValue("username", userName);

                        sqlComm.CommandType = CommandType.StoredProcedure;

                        sqlComm.ExecuteNonQuery();
                    }
                    Log.Information("Found user " + userName + " with the GUID: " + userGUID);
                }
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand sqlComm = new SqlCommand("getPlantbyGUID", conn);
                    sqlComm.Parameters.AddWithValue("userGUID", userGUID);

                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;

                    da.Fill(ds);

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Log.Information(sQLHelper.GetLocationFromID(row[0].ToString().ToLowerInvariant()));
                        return sQLHelper.GetLocationFromID(row[0].ToString().ToLowerInvariant());
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }
    }
}