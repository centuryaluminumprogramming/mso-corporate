﻿using Microsoft.Extensions.Configuration;
using MSO.DataAccess;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using System.Collections;
using iText.StyledXmlParser.Jsoup.Select;
using MSO.Controllers;

namespace MSO.Tools
{
    public class Print
    {
        private static IConfiguration _config;

        public Print(IConfiguration config)
        {
            _config = config;
        }

        public string createPdfFile(Models.Mso mso)
        {
            try
            {
                Files files = new Files(_config);
                string currentDirectory = Directory.GetCurrentDirectory();

                string pdfTemplate = currentDirectory + "\\ViewReport.pdf";
                string returnbool = "";

                var ourPlant = "";
                var destination = "";
                var otherSpecify = "";
                var charge = "";
                var allow = "";
                var collect = "";
                var cash = "";
                var credit = "";
                var creditMemo = "";
                var gatePass = "";
                var shipToExtended = "";
                var soldToExtended = "";

                /// Setting default path for printed file
                string newFile = currentDirectory + "\\Printed\\" + mso.RequestNumber + ".pdf";

                returnbool = mso.Return.ToString();

                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                AcroFields pdfFormFields = pdfStamper.AcroFields;

                if (mso.Vendor != null)
                {
                    shipToExtended = mso.Vendor.City + ", " + mso.Vendor.State + ", " + mso.Vendor.Zip;
                }
                soldToExtended = mso.SoldTo3 + ", " + mso.SoldTo4;

                if (!string.IsNullOrEmpty(mso.Fob.ToString()))
                {
                    switch (mso.Fob.ToString())
                    {
                        case "1":
                            ourPlant = "X";
                            break;

                        case "2":
                            destination = "X";
                            break;

                        case "3":
                            otherSpecify = "X";
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(mso.Freight.ToString()))
                {
                    switch (mso.Freight.ToString())
                    {
                        case "1":
                            charge = "X";
                            break;

                        case "2":
                            allow = "X";
                            break;

                        case "3":
                            collect = "X";
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(mso.TransactionType.ToString()))
                {
                    switch (mso.TransactionType.ToString())
                    {
                        case "1":
                            cash = Convert.ToInt32(mso.TransactionType) == 1 ? "X" : null;
                            break;

                        case "2":
                            credit = Convert.ToInt32(mso.TransactionType) == 2 ? "X" : null;
                            break;

                        case "3":
                            gatePass = Convert.ToInt32(mso.TransactionType) == 4 ? "X" : null;
                            break;

                        case "null":
                            break;
                    }
                }

                var vendorReturn = mso.Return == true ? "X" : null;
                var repaired = mso.Repaired == true ? "X" : null;
                var goods = mso.GoodsShipped == true ? "X" : null;
                var loan = mso.GoodsLoaned == true ? "X" : null;
                var removed = mso.VendorRemove == true ? "X" : null;
                var other = mso.Other == true ? "X" : null;

                var creationDate = (!string.IsNullOrEmpty(mso.CreationDate.ToString())) ? Convert.ToDateTime(mso.CreationDate).ToShortDateString() : Convert.ToDateTime(mso.ShipDate).ToShortDateString();

                pdfFormFields.SetField("PLANT CODE", mso.Location);
                pdfFormFields.SetField("MSO NO", mso.RequestNumber.ToString());
                pdfFormFields.SetField("MSO DATE", creationDate);
                pdfFormFields.SetField("MSO SHIP DATE", Convert.ToDateTime(mso.ShipDate).ToShortDateString());

                pdfFormFields.SetField("Purchase Order NO", mso.PurchaseNumber);
                pdfFormFields.SetField("Other Ref NO", mso.OtherReferenceNumber);
                pdfFormFields.SetField("Work Order Number", mso.WorkOrderNumber);

                pdfFormFields.SetField("REQ NO", mso.OrderNumber);
                pdfFormFields.SetField("BILL OF LADING NO", mso.BillOfLading);

                pdfFormFields.SetField("SHIP VIA", mso.ShipVia);
                pdfFormFields.SetField("SHIP NAME", mso.Vendor.Name);
                pdfFormFields.SetField("SHIP ADDRESS", mso.Vendor.Address);
                pdfFormFields.SetField("SHIP EXTENDED", shipToExtended);

                pdfFormFields.SetField("SOLD NAME", mso.SoldTo1);
                pdfFormFields.SetField("SOLD ADDRESS", mso.SoldTo2);
                pdfFormFields.SetField("SOLD EXTENDED", soldToExtended);

                pdfFormFields.SetField("Requested by NAME", mso.RequestedByName.ToString());
                pdfFormFields.SetField("Requested by DEPT", mso.RequestedByDepartment.ToString());

                pdfFormFields.SetField("AMOUNT", mso.CashAmount.ToString());
                pdfFormFields.SetField("RECEIPT NO", mso.CashReceipt.ToString());
                pdfFormFields.SetField("DATE RECEIVED", mso.CashReceived.ToString());
                pdfFormFields.SetField("CHECK NO", mso.CashCheckNo.ToString());
                pdfFormFields.SetField("VENDOR NO", mso.CorpVenNo);

                /// The form's checkboxes
                pdfFormFields.SetField("OUR PLANT", ourPlant.ToString());
                pdfFormFields.SetField("DESTINATION", destination);
                pdfFormFields.SetField("OTHER SPECIFY", otherSpecify);
                pdfFormFields.SetField("PREPAY CHARGE", charge);
                pdfFormFields.SetField("PREPAY ALLOW", allow);
                pdfFormFields.SetField("COLLECT", collect);

                var fieldType = pdfFormFields.GetFieldType("CASH SALE");
                if (cash != null)
                {
                    pdfFormFields.SetField("CASH SALE", cash);
                }

                if (credit != null)
                {
                    pdfFormFields.SetField("CREDIT SALE", credit);
                }
                if (creditMemo != null)
                {
                    pdfFormFields.SetField("CREDIT MEMO", creditMemo);
                }
                if (gatePass != null)
                {
                    pdfFormFields.SetField("GATE PASS", gatePass);
                }

                /// The form's Items
                SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);

                int itemcount = sQLHelper.GetMsoItems(Convert.ToInt32(mso.RequestNumber)).Count();

                foreach (var item in sQLHelper.GetMsoItems(Convert.ToInt32(mso.RequestNumber)))
                {
                    pdfFormFields.SetField("CA STOCK Row" + itemcount, item.StockNumber);
                    pdfFormFields.SetField("DescriptionRow" + itemcount, item.Description);
                    pdfFormFields.SetField("QtyRow" + itemcount, item.Quantity.ToString());
                    pdfFormFields.SetField("UnitRow" + itemcount, item.Unit);
                    pdfFormFields.SetField("Gross WGTRow" + itemcount, item.Gross.ToString());
                    pdfFormFields.SetField("Tare WGTRow" + itemcount, item.Tare.ToString());
                    pdfFormFields.SetField("Net WGTRow" + itemcount, item.Net.ToString());
                    pdfFormFields.SetField("Mat WGTRow" + itemcount, item.ItemCount.ToString());

                    itemcount = itemcount - 1;
                }

                pdfFormFields.SetField("Remarks", mso.Remarks);
                pdfFormFields.SetField("Shipping Instructions", mso.ShippingInstructions);
                pdfFormFields.SetField("Shipment Completed By", mso.CompletedBy);
                pdfFormFields.SetField("Buyer", mso.Buyer);
                pdfFormFields.SetField("NAME OF VENDORS REPRESENTITIVE", mso.ReturnRep);
                pdfFormFields.SetField("REPRESENTATIVE WHO AUTHORIZED RETURN", mso.CenturyRepresentative);

                /// report by reading values from completed PDF
                /// flatten the form to remove editting options, set it to false
                /// to leave the form open to subsequent manual edits
                pdfStamper.FormFlattening = false;
                /// close the pdf

                pdfStamper.Close();

                return (newFile);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return "";
            }
        }
    }
}