﻿using Microsoft.Extensions.Configuration;
using MSO.DataAccess;

//using MSO.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using System.DirectoryServices.AccountManagement;

namespace MSO.Tools
{
    public class Users
    {
        private static IConfiguration _config;
        private static string _MsoConnectionString;

        public Users(string MsoSQLConnection)
        {
            //MsoSQLConnection = Environment.GetEnvironmentVariable("MsoSQLConnection").ToString();
            _MsoConnectionString = MsoSQLConnection;
        }

        public Users(IConfiguration config)
        {
            _config = config;
        }

        public void UserName()
        {
            Models.User user = new Models.User();
            user.UserName = System.Security.Claims.ClaimsPrincipal.Current.Identity.Name;
        }

        public Models.User GetUserInfo(string userName)
        {
            Models.User user = new Models.User();

            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "GetUserInfo";
                    cmd.Parameters.AddWithValue("UserName", userName);

                    using (SqlDataReader rdr = cmd.ExecuteReader())

                    {
                        while (rdr.Read())
                        {
                            user.GUID = rdr["GUID"].ToString();
                            user.Plant = rdr["Plant"].ToString();
                            user.UserName = userName;
                            user.RoleId = Convert.ToInt32(rdr["RoleId"].ToString());
                        }
                    }
                }
                return user;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return user;
            }
        }

        public string GetUserGUID(string userName)
        {
            string userGUID;

            using (var context = new PrincipalContext(ContextType.Domain, "Century"))
            {
                UserPrincipal User = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);

                userGUID = Convert.ToString(User.Guid);
                Log.Information("Found user " + userName + " with the GUID: " + userGUID);
            }
            return userGUID;
        }

        public Plants InsertUser(Models.User user)
        {
            SQLHelper sQLHelper = new SQLHelper(_MsoConnectionString);
            sQLHelper.InsertUserPlant(user.UserName, user.GUID, user.Plant, user.RoleId);
            return null;
        }
    }
}