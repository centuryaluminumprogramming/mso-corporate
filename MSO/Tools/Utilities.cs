﻿using MSO.Models;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Data;
using MSO.DataAccess;
using System.Linq;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Http;

using System.Configuration;

using MSO.Tools;
using System.Net;

namespace MSO.Tools
{
    public class Utilities
    {
        private static IConfiguration _config;
        private static readonly StringBuilder sb = new StringBuilder();
        private static string _MsoConnectionString;

        public Utilities(string MsoSQLConnection)
        {
            _MsoConnectionString = MsoSQLConnection;
        }

        public Utilities(IConfiguration config)
        {
            _config = config;
        }

        public IList<Waste> BuildWasteFile(string StartDate, string EndDate)
        {
            try
            {
                IList<Waste> records = new List<Waste>();
                SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
                records = sQLHelper.WasteReport(StartDate, EndDate);

                return records;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public bool DownloadAdhocFile(string adhoc, string msoNumber, string StartDate, string EndDate, string user, string file)
        {
            bool hasData = false;
            int userLength = 0;

            userLength = Convert.ToInt32(user.Length.ToString());

            user = user.Substring(8, userLength - 8).ToString();

            try
            {
                var adhocend = adhoc.Length;
                var adhocstart = 0;
                var adhocLength = adhoc.Substring(adhocstart, (adhocend - 1));
                List<string> records = new List<string>();
                SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
                var data = sQLHelper.GetAdhoc(adhocLength, msoNumber, StartDate, EndDate);
                int colcount = 0;
                string adhoccolumn = "";

                foreach (DataRow colrow in data.Rows)
                {
                    DataColumnCollection columns = colrow.Table.Columns;
                    if (colcount == 0)
                    {
                        foreach (DataColumn column in columns)
                        {
                            if (String.IsNullOrEmpty(adhoccolumn))
                            {
                                adhoccolumn = column.ColumnName;
                            }
                            else
                            {
                                adhoccolumn = adhoccolumn + "," + column.ColumnName;
                            }
                        }

                        sb.AppendLine(adhoccolumn);
                        colcount = 1;
                    }
                }
                foreach (DataRow row in data.Rows)
                {
                    hasData = true;
                    List<string> fields = new List<string>(row.ItemArray.Select(field => field.ToString().Replace(",", "")));
                    sb.AppendLine(string.Join(",", fields));
                }
                if (hasData)
                {
                    if (File.Exists("C:\\Users\\" + user + "\\Downloads\\" + file + ".csv"))
                    {
                        File.Delete("C:\\Users\\" + user + "\\Downloads\\" + file + ".csv");
                    }

                    File.WriteAllText("C:\\Users\\" + user + "\\Downloads\\" + file + ".csv", string.Join(",", sb.ToString()));

                    Log.Information(user + " downloaded " + file + ".csv on " + "[ " + DateTime.Now + " ]");
                }

                return hasData;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " for " + user + " attempting to download " + file + ".csv");
                return hasData;
            }
        }

        public IList<Waste> DownloadWasteExcel(string StartDate, string EndDate, string user)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            IList<Waste> wastes = new List<Waste>();
            StringBuilder sb = new StringBuilder();

            int userLength = 0;
            userLength = Convert.ToInt32(user.Length.ToString());

            user = user.Substring(8, userLength - 8).ToString();
            EndDate = StartDate.Substring(11, 10).ToString();
            StartDate = StartDate.Substring(0, 10).ToString();

            try
            {
                wastes = BuildWasteFile(StartDate, EndDate);

                sb.AppendLine(string.Join(",", "MsoNumber,MsoDate,Hauler,Description,Weight"));

                foreach (Waste item in wastes)
                {
                    sb.AppendLine(string.Join(",", item.RequestNumber, item.CreationDate, item.Hauler.Replace(",", " ").ToString(), item.Description, item.Weight));
                }
                if (File.Exists("C:\\Users\\" + user + "\\Downloads\\wastereport.csv"))
                {
                    File.Delete("C:\\Users\\" + user + "\\Downloads\\wastereport.csv");
                }
                File.WriteAllText("C:\\Users\\" + user + "\\Downloads\\wastereport.csv", sb.ToString());

                //Log.Information("C:\\Users\\" + user + "\\Downloads\\wastereport.csv");
                Log.Information(user + " downloaded wastereport.csv on " + "[ " + DateTime.Now + " ]");

                return wastes;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " for " + user + " attempting to download wastereport.csv.");
                return wastes;
            }
        }

        public System.IO.Stream SendMail(Stream ReportData, string FileName, string ToAddress)
        {
            try
            {
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("webmail.centuryaluminum.com")
                {
                    EnableSsl = false,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential("processControl", "sendMyMail1")
                };

                System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage();
                System.IO.Stream ms = ReportData;
                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(ms, new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Plain));
                attachment.ContentDisposition.FileName = FileName;

                mm.To.Add(new MailAddress(ToAddress));
                mm.From = new System.Net.Mail.MailAddress("CustomReport@centuryaluminum.com");
                mm.Body = "Attached custom report.";
                mm.Subject = "MSO Custom Report";
                mm.Attachments.Add(attachment);

                smtp.Send(mm);

                return ms;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public static System.Web.Mvc.FileResult DownloadSpreadsheet(string file)
        {
            // Get the temp folder and file path in server
            string fullPath = Path.Combine(("c:/temp"), file);

            // Return the file for download, this is an Excel
            // so I set the file content type to "application/vnd.ms-excel"
            return new System.Web.Mvc.FilePathResult(Path.GetFileName(fullPath), "application/force-download");
        }

        public User GetUserInfo(string userName)
        {
            User user = new User();

            try
            {
                using (SqlConnection conn = new SqlConnection(_MsoConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "GetUserInfo";
                    cmd.Parameters.AddWithValue("UserName", userName);
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())

                    {
                        while (rdr.Read())
                        {
                            user.GUID = rdr["GUID"].ToString();
                            user.Plant = rdr["Plant"].ToString();
                            user.UserName = userName;
                            user.RoleId = Convert.ToInt32(rdr["RoleId"].ToString());
                        }
                    }
                }
                return user;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return user;
            }
        }

        public Plant UpdatePlant(string plant, User user)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            sQLHelper.UpdatePlant(user.UserName, user.GUID, plant);
            return null;
        }

        public Plant InsertUser(User user)
        {
            SQLHelper sQLHelper = new SQLHelper(_config["MsoSQLConnection"]);
            sQLHelper.InsertUserPlant(user.UserName, user.GUID, user.Plant, user.RoleId);
            return null;
        }

        public string GetUserGUID(string userName)
        {
            string userGUID;

            using (var context = new PrincipalContext(ContextType.Domain, "Century"))
            {
                UserPrincipal User = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);

                userGUID = Convert.ToString(User.Guid);
                Log.Information("Found user " + userName + " with the GUID: " + userGUID);
            }
            return userGUID;
        }

        public static bool IsLocal(string host)
        {
            // get host IP addresses
            IPAddress[] hostIPs = Dns.GetHostAddresses(host);
            // get local IP addresses
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

            // test if any host IP equals to any local IP or to localhost
            foreach (IPAddress hostIP in hostIPs)
            {
                // is localhost
                if (IPAddress.IsLoopback(hostIP)) return true;
                // is local address
                foreach (IPAddress localIP in localIPs)
                {
                    if (hostIP.Equals(localIP)) return true;
                }
            }
            return false;
        }

        //public static System.IO.Stream GenerateStreamFromString(List<Waste>s)
        //{
        //    try
        //    {
        //        StringBuilder sb = new StringBuilder();
        //        s.ForEach(str => sb.AppendLine(str));

        //        System.IO.Stream stream = new System.IO.MemoryStream();
        //        System.IO.StreamWriter writer = new System.IO.StreamWriter(stream);
        //        foreach (var item in s)
        //        {
        //            writer.WriteLine(item.ToString());
        //            writer.Flush();
        //        }
        //        stream.Position = 0;
        //        return stream;
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex.Message);
        //        return null;
        //    }
        //}
    }
}